var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

users = [];
connections = [];

server.listen(3000);
console.log('Server running... on port ' + server.address().port);

io.sockets.on('connection', function (socket) {
    var userId;
    var donationMemberId;

    connections.push(socket);
    console.log('%s connected sockets', connections.length);
    io.sockets.emit('update-users-online', connections.length);

    socket.on('disconnect', function (socket) {
        connections.splice(connections.indexOf(socket), 1);
        console.log('%s connected sockets', connections.length);
        io.sockets.emit('update-users-online', connections.length);
    });

    socket.on('send-new-signal', function (data) {
        console.log('send-new-signal');
        io.sockets.emit('new-signal', data);
    });

    socket.on('save-user-id', function (id) {
        userId = id;
        console.log('user id :' + userId);

        // //accounts
        // socket.on('user-account-count-update-' + userId, function (data){
        //     console.log('user-account-count-update-' + userId);
        //     io.sockets.emit('user-added-account-' + userId, data);
        // });
        //
        // //complaints
        // socket.on('user-complaint-count-update-' + userId, function (data){
        //     console.log('user-complaint-count-update-' + userId);
        //     io.sockets.emit('user-added-complaint-' + userId, data);
        // });
        //
        // //transactions
        // socket.on('user-transactions-count-update-' + userId, function (data){
        //     console.log('user-transactions-count-update-' + userId);
        //     console.log(userId, data.memberId, data.donationId);
        //     io.sockets.emit('user-added-transaction-' + userId, data);
        // });
        //
        // // //donations reserved
        // socket.on('user-'+userId+'-donation-reserved', function (data){
        //     console.log('user-'+userId+'-donation-reserved');
        //     io.sockets.emit('user-update-donations-' + data.memberId, data);
        // });
        //
        // // user update profile
        // socket.on('user-profile-update-'+userId, function (data){
        //     console.log('user-profile-update-'+userId);
        //     io.sockets.emit('user-did-update-' + data.id, data);
        // });
    })
});