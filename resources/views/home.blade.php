@extends('layouts.app')

@section('content')
    <div id="user_id" hidden>{{Auth::user()->id}}</div>
    <div id="home" hidden>{{ URL::to('/home')}}</div>
    @if(Auth::user()->status_is=='Active')
        <div class="row">
            <div class="col l4 m4 s12">
                <div class="card teal">
                    <div class="card-content white-text">
                        <p class="card-stats-title"><i class="mdi-social-group-add teal white-text"></i> Followers</p>
                        <h4 class="card-stats-number">{{\Illuminate\Support\Facades\Auth::user()->number_of_followers}}</h4>
                    </div>
                    <div class="card-action  teal darken-2 white-text">
                        Your Referral Link: <br/><a class="truncate" style="text-transform: none;" 
                                href="{{ URL::to('follow/me/'. $user->id) }}">{{ URL::to('follow/me/'. $user->id) }}</a>
                    </div>
                </div>
            </div> <!-- col l4 m4 s12 -->
            <div class="col l4 m4 s12">
                    @if($subscription->status_is == "Active")
                <div class="card green">
                    <div class="card-content  green white-text">
                        <p class="card-stats-title"><i class="mdi-action-done green white-text"></i> Subscription Status</p>
                        <h4 class="card-stats-number">{{ $subscription->status_is}}</h4>
                    </div>
                    <div class="card-action  green darken-2 white-text">
                        Expiry Date {{ $subscription->expiry_date }}<br/>
                        <br>
                    </div>
                </div>
                    @else

                <div class="card red">
                    <div class="card-content white-text">
                        <p class="card-stats-title"><i class="mdi-content-clear red white-text"></i> Subscription Status</p>
                        <h4 class="card-stats-number">{{ $subscription->status_is}}</h4>
                        </p>
                    </div>
                    <div class="card-action  red darken-2 white-text">
                        <div class="row">
                            <div class="col s12 m8 l8">
                                    Expiry Date {{ $subscription->expiry_date }}<br/><a href="{{ url('payments/make') }}" class="pull-right">Activate</a>
                            </div>
                            <div class="col s12 m4 l4">
                                Amount Due <br>
                                {{ round($amount, 2) }}
                            </div>
                        </div>
                    </div>
                </div>
                    @endif
            </div> <!-- col l4 m4 s12 -->
            <div class="col l4 m4 s12">

                <div class="card blue">
                    <div class="card-content white-text">
                        <p class="card-stats-title"><i class="mdi-action-payment blue white-text"></i> Commission</p>
                        <h4 class="card-stats-number">${{ $commission }}</h4>
                        </p>
                    </div>
                    <div class="card-action  blue darken-2 white-text">
                    <div class="row">
                        <div class="col s6 m6 l6">
                            Last Withdrawal <br>
                        @if($amount != null)
                            @if($commission > 0)
                            <form method='POST' action='{{url("withdraw")}}'>
                                {{ csrf_field() }}
                                <input type="hidden" name="commission" value="{{ $commission }}"/>
                                <input type="hidden" name="commissions" value="{{ $commissions }}"/>

                                <button class='btn link orange-text' type="submit">Withdraw</button>
                            </form>
                            @endif
                        @endif
                        </div>
                        <div class="col s6 m6 l6">
                            <br>
                            <a href="{{ url('/withdrawals') }}" class="right">History</a>
                        </div>
                    </div>
                        
                        
                    </div>
                </div>
            </div> <!-- col l4 m4 s12 -->
        </div>
        <div class="row">
            {{-- Hidden if user blocked or account not active --}}
            <div class="col l8 m6 s12">
                <h4 class="header">Signals</h4>
                @if($signals->count() > 0)
                <ul class="collection z-depth-1" id="mySignals">
                    @foreach($signals as $signal)
                        <li class="collection-item avatar">
                            <div class="row">
                                <div class="col s12 blue-grey-text">
                                    @if($signal->action_is ==  'buy')
                                        <i class="mdi-action-trending-up circle green"></i>
                                    @else
                                        <i class="mdi-action-trending-down circle red"></i>
                                    @endif
                                    <span class="collection-header"><strong>{{ ucwords($signal->action_is) }} {{ strtoupper($signal->pair_is) }}  {{ $signal->entry_is}}</strong></span>
                                    <p>
                                        <strong>SL </strong>{{$signal->stoploss_is}} <br>
                                        <strong>TP</strong> {{$signal->takeprofit_is}}
                                        <br/>

                                    </p>
                                    <p class="right-align"><i
                                                class="mdi-action-schedule"></i> {{ $signal->created_at->diffForHumans() }}
                                    </p>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
                @else
                @include('layouts.empty-content')
                @endif
            </div>
            <div class="col l4 m6 s12">

                <h4 class="header">Tips</h4>
                <ul class="collection">
                    <li class="collection-item avatar">
                        <i class="circle mdi-social-person orange darken-2"></i>
                        <p><span><strong>Signed In Members:</strong>
                        <p id="onlineUsers"></p></span>
                        </p>
                    </li>
                </ul>
                <ul class="collection">
                    <li class="collection-item avatar">
                        <i class="circle mdi-action-done green darken-2"></i>
                        <p><span><strong>Security:</strong> Regularly change your password to increase...</span>
                        </p>
                    </li>
                </ul>
                <ul class="collection hide">
                    <li class="collection-item avatar">
                        <i class="circle mdi-action-trending-up light-blue darken-2"></i>
                        <span class="collection-header">Market</span>
                    </li>
                    <li class="collection-item" style="padding: 0;"><iframe frameborder="0" scrolling="auto" height="652" style="width: 100%;" allowtransparency="true" marginwidth="0" marginheight="0" src="http://fxrates.za.forexprostools.com/index_single_crosses.php?currency=12&header-text-color=%23080808&header-bg=%23ffffff&curr-name-color=%230059b0&inner-text-color=%23000000&green-text-color=%232A8215&green-background=%23B7F4C2&red-text-color=%23DC0001&red-background=%23FFE2E2&inner-border-color=%23CBCBCB&border-color=%23cbcbcb&bg1=%23F6F6F6&bg2=%23ffffff&bid=show&ask=show&last=hide&open=hide&high=hide&low=hide&change=hide&change_in_percents=show&last_update=hide"></iframe><br /><div style="width:277"><span style="float:right"><span style="font-size: 11px;color: #333333;text-decoration: none;">The Forex Quotes are powered by <a href="http://za.investing.com/" rel="nofollow" target="_blank" style="font-size: 11px;color: #06529D; font-weight: bold;" class="underline_link">Investing.com ZA</a>.</span></span></div></li>
                </ul>
                <ul class="collection">
                    <li class="collection-item avatar">
                        <i class="circle mdi-communication-message gray lighten-2"></i>
                        <span class="collection-header">Headlines</span>
                    </li>
                    <li class="collection-item">Trump's victory, dollar hits all time highs</li>
                </ul>
            </div>
        </div>
    @endif
@endsection
@section('scripts')
    <script type="application/javascript">
        var user_id = $('#user_id').text();
        var home = $('#home').text();

        socket.on('new-signal', function (data) {
            console.log('new-signal');
            console.log(data);
            console.log(JSON.parse(data["eligibleUsers"]));

            var eligableUsers = data["eligibleUsers"];
            console.log(eligableUsers.indexOf(user_id));
            console.log(user_id);
            if (eligableUsers.indexOf(user_id) > 0) {
                var info = JSON.parse(data["signal"]);

                var classToAppend = "mdi-action-trending-down circle red";
                var notificationImage = 'images/bear_trend.png';
                if (info["action_is"] == "buy") {
                    classToAppend = "mdi-action-trending-up circle green";
                    notificationImage = 'images/bull_trend.png';
                }

                var li = '<li class="collection-item avatar"><div class="row"><div class="col s12 blue-grey-text">' +
                        '<i class="' + classToAppend + '"></i><span class="collection-header">' +
                        '<strong>' + capitalizeFirstLetter(info["action_is"]) + ' ' + info["pair_is"].toUpperCase() + ' ' + info["entry_is"] + '</strong></span>' +
                        '<p><strong>SL </strong> ' + info["stoploss_is"] + ' <br><strong>TP </strong> ' + info["takeprofit_is"] + '<br/></p> ' +
                        '<p class="right-align"><i class="mdi-action-schedule"></i> Just now..</p> </div> </div> </li>';

                console.log(li);

                $(li).hide().prependTo("#mySignals").hide().slideDown();


                if (Notification.permission == "default") {
                    Notification.requestPermission(function (p) {
                    });
                } else {
                    var signal = info["action_is"] + ' ' + info["pair_is"] + ', Entry:' + info["entry_is"] + ', \nTP ' + info["takeprofit_is"] + ', SL ' + info["stoploss_is"];
                    var display = signal.toUpperCase();

                    var notify = new Notification('New HTC signal notification', {
                        body: display,
                        icon: notificationImage
                    });

                    notify.onclick = function () {
                        window.location = home;
                        window.focus();
                        notify.close();
                    };
                }

                function capitalizeFirstLetter(string) {
                    return string.charAt(0).toUpperCase() + string.slice(1);
                }
            }
        });
    </script>
@endsection
