<div class="row">
    <div class="col s12">
        <div class="center-align">
            <i class="grey-text {{ $icon or 'mdi-content-report'}} text-lighten-2 large"></i>
            
        </div>
        <h4 class="medium center grey-text text-lighten-2">{!! $message or 'Nothing to display!' !!}</h4>
    </div>
</div>