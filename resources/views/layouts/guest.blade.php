<!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 3.1
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>HTC - High Trade Commissions</title>

    <!-- Favicons-->
    <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->
    {!! Html::style('css/materialize.css') !!}
    {!! Html::style('css/style.css') !!}
    <!-- Custome CSS-->
    {!! Html::style('css/custom/custom.css') !!}
    {!! Html::style('css/layouts/page-center.css') !!}

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    {!! Html::style('js/plugins/prism/prism.css') !!}
    {!! Html::style('js/plugins/perfect-scrollbar/perfect-scrollbar.css') !!}

</head>

<body class="grey">
<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->


@yield('content')


<!-- ================================================
Scripts
================================================ -->

<!-- jQuery Library -->
{!! Html::script('js/plugins/jquery-1.11.2.min.js') !!}
<!--materialize js-->
{!! Html::script('js/materialize.js') !!}
<!--prism -->
{!! Html::script('js/prism/prism.js') !!}
<!--scrollbar-->
{!! Html::script('js/plugins/perfect-scrollbar/perfect-scrollbar.min.js') !!}

<!--plugins.js - Some Specific JS codes for Plugin Settings-->
{!! Html::script('js/plugins.js') !!}
<!--custom-script.js - Add your own theme custom JS-->
{!! Html::script('js/custom-script.js') !!}

</body>

</html>