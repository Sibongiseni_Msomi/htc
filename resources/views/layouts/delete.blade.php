<button class='viewShow btn-flat waves-effect waves-light grey-text delete-btn{{ $id }}' style="padding: 0 10px;"><i class="mdi-action-delete"></i></button>
<form class="hidden" method="POST" action="{{ url()->current().'/'.$id }}" id="delete-form{{ $id }}" style="display: inline-block">
    <input type="hidden" name="_method" value="DELETE">
    {{ csrf_field() }}
 @php $name = empty($name) ? 'item' : $name @endphp

<script>
jQuery(document).ready(function(){
        jQuery('.delete-btn{{ $id }}').on('click', function(e) {
            e.preventDefault();
        	swal({
        		title: "Are you sure?",
        		text: "You will not be able to recover this {{ $name }}?",
        		type: "warning",
        		showCancelButton: true,
        		confirmButtonColor: '#DD6B55',
        		confirmButtonText: 'Yes, delete it!',
        		closeOnConfirm: false
        	},
        	function(isConfirm){
                if(isConfirm){
                    jQuery('#delete-form{{ $id }}').submit();
                    swal("Done!", "{{ $name }} was succesfully deleted!", "success");
                }else{
                    swal("Error deleting!", "Please try again", "error");
                }
        	});
        });
    });

</script>

</form>
