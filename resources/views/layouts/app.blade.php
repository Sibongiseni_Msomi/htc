<!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 3.1
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description"
          content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords"
          content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>{{ $page['page'] or 'HTC' }} - High Trade Commissions</title>

    <!-- Favicons-->
    <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->

    <!-- CORE CSS-->
    {!! Html::style('css/materialize.css') !!}
    {!! Html::style('css/style.css') !!}
    <!-- Custome CSS-->
    {!! Html::style('css/custom/custom.css') !!}

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    {!! Html::style('js/plugins/prism/prism.css') !!}
    {!! Html::style('js/plugins/perfect-scrollbar/perfect-scrollbar.css') !!}
    {!! Html::style('js/plugins/chartist-js/chartist.min.css') !!}

    <!-- jQuery Library -->
    {!! Html::script('js/plugins/jquery-1.11.2.min.js') !!}
    <!-- Socket Scripts-->
    {!! Html::script('http://35.162.236.82:3000/socket.io/socket.io.js') !!}
    {!! Html::script('js/notification.js') !!}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" integrity="sha256-iXUYfkbVl5itd4bAkFH5mjMEN5ld9t3OHvXX3IU8UxU=" crossorigin="anonymous" />


</head>

<body>
<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->
<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START HEADER -->
<header id="header" class="page-topbar">
    <!-- start header nav-->
    <div class="navbar-fixed">
        <nav class="navbar-color">
            <div class="nav-wrapper">
                <ul class="left">
                    <li><h1 class="logo-wrapper"><a href="{{ url('/') }}" class="brand-logo darken-1"><img
                        src="{{ url('/' . 'images/materialize-logo.png') }}" alt="materialize logo"></a>
                            <span class="logo-text">High Trade Commissions</span></h1>
                    </li>
                </ul>
                <ul class="right hide-on-med-and-down">
                    <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i
                                    class="mdi-action-settings-overscan"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- end header nav-->
</header>
<!-- END HEADER -->

<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START MAIN -->
<div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav">
            <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                    <div class="row">
                        <div class="col col s4 m4 l4">
                            <span class="circle red darken-1 z-depth-1 white-text alphatar truncate">{{ substr(\Auth::user()->firstname, 0, 1) }}</span>
                        </div>
                        <div class="col col s8 m8 l8">
                            <ul id="profile-dropdown" class="dropdown-content">
                                <li><a href="{{ url('/users/'.Auth::user()->id) }}"><i class="mdi-action-face-unlock"></i> Profile</a>
                                </li>
                                @if(false)
                                <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                                </li>
                                <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                                </li>
                                @endif
                                <li class="divider"></li>
                                <li><a href="{{ url('logout') }}"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                                </li>
                            </ul>
                            <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#"
                               data-activates="profile-dropdown">{{ Auth::user()->firstname }}<i
                                        class="mdi-navigation-arrow-drop-down right"></i></a>
                            <p class="user-roal">{{ Auth::user()->contactnumber }}</p>
                        </div>
                    </div>
                </li>
                <li class="bold"><a href="{{ url('/home') }}" class="waves-effect waves-cyan"><i
                                class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i
                                        class="mdi-file-folder-shared"></i> Profile & Accounts</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="{{ url('/users/' . Auth::user()->id .'/edit') }}">Update Profile</a>
                                    </li>
                                    <li><a href="{{ url('/accounts') }}">My Accounts</a>
                                    </li>
                                    <li><a href="{{ url('/users/' . Auth::user()->id .'/edit') }}">Change Password</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                @if(Auth::user()->verified==1)
                <li class="bold"><a href="{{ url('/signals') }}" class="waves-effect waves-cyan"><i
                                class="mdi-action-announcement"></i> Signals</a>
                </li>
                @endif
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i
                                        class="mdi-action-credit-card"></i> Billing</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="{{ url('/invoices') }}">Invoices</a>
                                    </li>
                                    <li><a href="{{ url('/withdrawals') }}">Withdrawals</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>

                <li class="bold"><a href="{{ url('/followers') }}" class="waves-effect waves-cyan"><i
                                class="mdi-social-people"></i> Genealogy</a>
                </li>

                @if(Auth::user()->hasRole('admin'))
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i
                                        class="mdi-action-wallet-membership"></i> Payments</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="/payments/paypal-payments">PayPal Payments</a>
                                    </li>
                                    <li><a href="#!">SagePay Payments</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                @endif

                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i
                                        class="mdi-action-settings"></i> Settings</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="{{ url('/settings/forex-pairs') }}">Forex Pairs</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
            <a href="#" data-activates="slide-out"
               class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i
                        class="mdi-navigation-menu"></i></a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->
        <section id="content">

            <!--breadcrumbs start-->
            <div id="breadcrumbs-wrapper">
                <!-- Search for small screen -->
                <div class="header-search-wrapper grey hide">
                    <i class="mdi-action-search active"></i>
                    <input type="text" name="Search" class="header-search-input z-depth-2"
                           placeholder="">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <h5 class="breadcrumbs-title">{{ $page['page'] }}</h5>
                            <ol class="breadcrumbs">
                                <li><a href="{{ url('/home') }}">Dashboard</a></li>
                                <li><a href="{{ url('/' . $page['back']) }}">{{ $page['page'] }}</a></li>
                                <li class="active">{{ $page['current'] }}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!--breadcrumbs end-->


            <!--start container-->
            <div class="container">
                <div class="section">
                    @if (session()->has('flash_notification.message'))
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <div id="card-alert" class="card {!! session('flash_notification.level') !!}">
                                    <div class="card-content white-text">
                                        <p><i class="mdi-action-info-outline"></i> {!! session('flash_notification.message') !!}</p>
                                    </div>
                                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if(Auth::user()->verified==1)
                        @yield('content')
                    @else
                        @include('layouts.verification')
                    @endif
                    
                    <br><br><br>
                    <div class="divider"></div>
                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
                <!-- Floating Action Button -->
                <div class="fixed-action-btn hide" style="bottom: 50px; right: 19px;">
                    <a class="btn-floating btn-large">
                        <i class="mdi-action-stars"></i>
                    </a>
                    <ul>
                        <li><a href="css-helpers.html" class="btn-floating red"><i
                                        class="large mdi-communication-live-help"></i></a></li>
                        <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i
                                        class="large mdi-device-now-widgets"></i></a></li>
                        <li><a href="app-calendar.html" class="btn-floating green"><i
                                        class="large mdi-editor-insert-invitation"></i></a></li>
                        <li><a href="app-email.html" class="btn-floating blue"><i
                                        class="large mdi-communication-email"></i></a></li>
                    </ul>
                </div>
                <!-- Floating Action Button -->
            </div>
            <!--end container-->
        </section>
        <!-- END CONTENT -->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

    </div>
    <!-- END WRAPPER -->

</div>
<!-- END MAIN -->


<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START FOOTER -->
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            <span>Copyright © {{ date("Y")}} <a class="grey-text text-lighten-4"
                                      href="#!"
                                      target="_blank">High Trade Commissions</a> All rights reserved.</span>
            <span class="right">Developed by <a class="grey-text text-lighten-4"
                                                            href="#!"><span style="-moz-transform: scale(-1, 1);-webkit-transform: scale(-1, 1);
-o-transform: scale(-1, 1);-ms-transform: scale(-1, 1);transform: scale(-1, 1);display:inline-block;">4</span>Dudes & 1 Chick</a></span>
        </div>
    </div>
</footer>
<!-- END FOOTER -->


<!-- ================================================
Scripts
================================================ -->


    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" integrity="sha256-egVvxkq6UBCQyKzRBrDHu8miZ5FOaVrjSqQqauKglKc=" crossorigin="anonymous"></script>
<!--materialize js-->
{!! Html::script('js/materialize.js') !!}
<!--prism
{!! Html::script('js/prism/prism.js') !!} -->
<!--scrollbar-->
{!! Html::script('js/plugins/perfect-scrollbar/perfect-scrollbar.min.js') !!}
<!-- chartist -->
{!! Html::script('js/plugins/chartist-js/chartist.min.js') !!}

<!--plugins.js - Some Specific JS codes for Plugin Settings-->
{!! Html::script('js/plugins.js') !!}
<!--custom-script.js - Add your own theme custom JS-->
{!! Html::script('js/custom-script.js') !!}
<div id="toast-container"></div>
</body>
<script>
    var socket = io.connect('http://35.162.236.82:3000');

    socket.on('update-users-online', function (numberOfOnlineUsers) {
//        document.getElementById("onlineUsers").innerHTML = numberOfOnlineUsers + ' member(s)';
        socket.emit('save-user-id', '4');
    });

    if (!window.Notification) {
        alert('sorry, notifications not supported');
    } else {
        Notification.requestPermission(function (p) {
        });
    }


</script>

@yield('scripts')

</html>
