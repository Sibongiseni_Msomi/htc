
<div class="verification-overlay">
    <div class="overlay-content">
        <div class="row">
            <div class="col s12 m10 l6 offset-l3 offset-m1">
                <div class="card z-depth-1-half">
                    <div class="card-title red darken-1 white-text" style="overflow:hidden; padding: 0 20px;">
                        <h3 class="center"><i class="mdi-action-lock-outline large"></i><br>Account Verification Required
                        </h3>
                    </div>
                    <div class="card-content">
                        <form  class="col s12" role="form" action="{{ url('/verification') }}" method="post">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="mdi-action-lock-outline prefix grey-text"></i>
                                <input id="verification" name="verification" type="text" class="validate">
                                <label for="verification" class="">Enter verification code</label>
                                @if ($errors->has('verification'))
                                    <span class="red-text">
                                        <strong>{{ $errors->first('verification') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col l12">
                                <button type="submit" class="btn light-blue waves-effect waves-light">Submit
                                </button>
                                <a class="btn red waves-effect waves-light" data-dismiss="modal"
                                href="{{ url('/resend') }}">Resend
                                    Code</a>
                                <br><br>
                            </div>
                        </div>          
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>