@extends('layouts.guest')

@section('content')
    <div id="login-page" class="row">
        <div class="col s12 z-depth-4 card-panel">
            <form class="register-form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="input-field col s12 center">
                        {!! Html::image('images/login-logo.png', 'High Trade Commissions', ['class'=>"circle responsive-img valign profile-image-login"]) !!}
                        <p class="center login-form-text">High Trade Commissions</p>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 center">
                        <h4>Register</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m6 l6">
                        <div class="row margin{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <div class="input-field col s12">
                                <i class="mdi-social-person-outline prefix"></i>
                                <input id="firstname" name="firstname" type="text">
                                <label for="firstname" class="center-align">Firstname</label>

                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="row margin{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <div class="input-field col s12">
                                <i class="mdi-social-person-outline prefix"></i>
                                <input id="lastname" name="lastname" type="text">
                                <label for="lastname" class="center-align">Lastname</label>

                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>                
                </div>

                <div class="row">
                    <div class="col s12 m6 l6">
                        <div class="row margin{{ $errors->has('contactnumber') ? ' has-error' : '' }}">
                            <div class="input-field col s12">
                                <i class="mdi-communication-call prefix"></i>
                                <input id="contactnumber" name="contactnumber" type="text">
                                <label for="contactnumber" class="center-align">Phone</label>

                                @if ($errors->has('contactnumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contactnumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="row margin{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="input-field col s12">
                                <i class="mdi-communication-email prefix"></i>
                                <input id="email" name="email" type="email">
                                <label for="email" class="center-align">Email</label>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m6 l6">
                        <div class="row margin{{ $errors->has('country_is') ? ' has-error' : '' }}">
                            <div class="input-field col s12">
                                <i class="mdi-social-public prefix"></i>
                                {!! Form::select('country_is', $countries, null, ['id'=>'country_is']) !!}
                                {!! Form::label('country_is', 'Country :') !!}
                                @if ($errors->has('country_is'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country_is') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="row margin{{ $errors->has('timezone_is') ? ' has-error' : '' }}">
                            <div class="input-field col s12">
                                <i class="mdi-action-language prefix"></i>
                                {!! Form::select('timezone_is', $timezones, null, ['id'=>'timezone_is']) !!}
                                {!! Form::label('timezone_is', 'Timezone :') !!}
                                @if ($errors->has('timezone_is'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('timezone_is') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <div class="row margin{{ $errors->has('package') ? ' has-error' : '' }}">
                            <div class="input-field col s12">
                                <i class="mdi-content-archive prefix"></i>
                                {!! Form::select('package', $packages, null, ['id'=>'package']) !!}
                                {!! Form::label('package', 'Package :') !!}
                                @if ($errors->has('package'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('package') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m6 l6">
                        <div class="row margin{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="input-field col s12">
                                <i class="mdi-action-lock-outline prefix"></i>
                                <input id="password" name="password" type="password">
                                <label for="password">Password</label>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="row margin{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <div class="input-field col s12">
                                <i class="mdi-action-lock-outline prefix"></i>
                                <input id="password_confirmation" name="password_confirmation" type="password">
                                <label for="password_confirmation">Password again</label>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m6 l6">
                        <div class="input-field col s12">
                            <button type="submit" class="btn waves-effect waves-light col s12">Register Now</button>
                        </div>                
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="input-field col s12">
                            <p class="margin center medium-small sign-up">Already have an account? <a href="{{ url('/login') }}">Login</a></p>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
