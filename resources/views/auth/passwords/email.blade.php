@extends('layouts.guest')

<!-- Main Content -->
@section('content')
    <div id="login-page" class="row">
        <div class="col s12 z-depth-4 card-panel">
            <form class="login-form" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="input-field col s12 center">
                        {!! Html::image('images/login-logo.png', 'High Trade Commissions', ['class'=>"circle responsive-img valign profile-image-login"]) !!}
                        <p class="center login-form-text">High Trade Commissions</p>
                    </div>
                </div>
                @if (session('status'))
                    <div class="alert alert-success">
                        <center>{{ session('status') }}</center>
                    </div>
                @endif
                <div class="row margin{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="input-field col s12">
                        <i class="mdi-social-person-outline prefix"></i>
                        <input id="email" name="email" type="text" value="{{ old('email') }}">
                        <label for="email" class="center-align">Email</label>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button type="submit" class="btn waves-effect waves-light col s12">Send Password Reset Link</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
