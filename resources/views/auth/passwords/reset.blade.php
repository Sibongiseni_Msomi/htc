@extends('layouts.guest')

@section('content')
    <div id="login-page" class="row">
        <div class="col s12 z-depth-4 card-panel">
            <form class="login-form" method="POST" action="{{ url('/password/reset') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="input-field col s12 center">
                        {!! Html::image('images/login-logo.png', 'High Trade Commissions', ['class'=>"circle responsive-img valign profile-image-login"]) !!}
                        <p class="center login-form-text">High Trade Commissions</p>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 center">
                        <h4>Reset Password</h4>
                    </div>
                </div>
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="row margin{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="input-field col s12">
                        <i class="mdi-communication-email prefix"></i>
                        <input id="email" name="email" type="email" value="{{ old('email') }}">
                        <label for="email" class="center-align">Email</label>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <div class="row margin{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="input-field col s12">
                        <i class="mdi-action-lock-outline prefix"></i>
                        <input id="password" name="password" type="password">
                        <label for="password">Password</label>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <div class="row margin{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <div class="input-field col s12">
                        <i class="mdi-action-lock-outline prefix"></i>
                        <input id="password_confirmation" name="password_confirmation" type="password">
                        <label for="password_confirmation">Password again</label>

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button type="submit" class="btn waves-effect waves-light col s12">Reset Password</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
