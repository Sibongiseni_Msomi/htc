@extends('layouts.app')

@section('content')

@if(\Auth::user()->hasRole('admin'))

    <div class="row">
        <div class="col s12 m10 l10 offset-l1 offset-m1">
            <br>

            <div class="card">
                <table class="table striped responsive-table">
                    <thead>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Contact Number</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>

                            <td data-label="First Name">{{$user->firstname}}</td>

                            <td data-label="Last Name">{{$user->lastname}}</td>

                            <td data-label="Contact Number">{{$user->contactnumber}}</td>

                            <td data-label="Email">{{$user->email}}</td>

                            <td data-label="Actions">
                                {!! Btn::delete($user->id) !!}
                                <a href = '{{url('/users/'.$user->id.'/edit')}}' class = 'viewEdit btn-flat black-text'  style="padding: 0 10px;"><i class="mdi-editor-mode-edit" aria-hidden="true"></i></a>
                                <a href="{{url('/users/'.$user->id)}}" class='viewShow btn-flat black-text' style="padding: 0 10px;"><i class="mdi-action-visibility" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
      </div>


@else
    <div class="row">
        <div class="col s12 l10 m12 offset-l1">
            <div id="profile-page-header" class="card">
                <div class="card-image waves-effect waves-block waves-light" style="position: relative">
                    <img class="activator" src="{{ url('/' . 'images/user-profile-bg.jpg') }}" alt="user background">                    
                </div>
                @if(Auth::user()->hasRole('admin') || $user->id == Auth::user()->id)
                <form method='get' action='{{url('/users/'.$user->id.'/edit')}}'>
                    <button style="position: absolute; top: 220px; right: 10px" class='btn-floating btn-large activator btn-move-up waves-effect waves-light darken-2 right red accent-4'><i class="mdi-editor-mode-edit"></i></button>
                </form>
                @endif
                <figure class="card-profile-image">
                       <span class="circle light-blue accent-4 darken-1 z-depth-2 white-text alphatar truncate" style="line-height: 110px; width:110px; font-size: 3rem;">{{ substr($user->firstname, 0, 1) }}</span>
                </figure>
                <div class="card-content">
                  <div class="row">
                    <div class="col s12 m4 l4 offset-m2 offset-l2">                        
                        <h4 class="card-title grey-text text-darken-4 truncate center-on-small-only">{{ $user->firstname }} {{ $user->lastname }}</h4>
                        <p class="medium-small grey-text center-on-small-only">Joined {{ $user->created_at->diffForHumans() }}</p>                        
                    </div>
                    <div class="col s6 m2 l2">
                        <h4 class="card-title grey-text text-darken-3 center truncate">Followers</h4>
                        <p class="medium-small grey-text center">{{ $user->number_of_followers }}</p>
                    </div>
                    @if(Auth::user()->hasRole('admin') || $user->id == Auth::user()->id)
                    <div class="col s6 m3 l3">
                        <h4 class="card-title grey-text center text-darken-3 truncate">Commission</h4>
                        <p class="medium-small grey-text center">{{ $commission }}</p>
                    </div>
                    @endif
                  </div>
                </div>
                <br>
                <div class="card-content" style="transform: translateY(0px);">
                    <div class="row">
                        <div class="col s12 m12 l9 offset-l2">
                            @if(Auth::user()->hasRole('admin') || $user->id == Auth::user()->id)   
                            <p><i class="mdi-action-perm-phone-msg cyan-text text-darken-2"></i> {{ $user->contactnumber }}</p>
                            <p><i class="mdi-communication-email cyan-text text-darken-2"></i> {{ $user->email }}</p>
                            @endif
                            <p><i class="mdi-social-public cyan-text text-darken-2"></i> {{ $user->country_is }} </p>
                            <p><i class="mdi-action-language cyan-text text-darken-2"></i> {{ $user->timezone_is }}</p>
                            @if(Auth::user()->hasRole('admin') || $user->id == Auth::user()->id)
                            <div class="divider"></div>
                            <p><i class="mdi-mdi-notification-sync-problem
                            cyan-text text-darken-2"></i> <span class="grey-text">Last updated {{ $user->updated_at }}</span></p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@if(Auth::user()->hasRole('admin') || $user->id == Auth::user()->id)
<div class="row">
    <div class="col s12 m12 l10 offset-l1">
    <div class="card orange white-text">
        <div class="card-content center">
            <h1 class="center white-text"><i class="mdi-content-archive large"></i></h1>
            <h4>Package</h4>
            <p>{{$sub->Package->name}}</p>
            <p>{{$sub->Package->features}}</p>
            <p>{{$sub->Package->amount}}</p>
        </div>
    </div>
    </div>
</div>
@endif
@endif
@endsection
