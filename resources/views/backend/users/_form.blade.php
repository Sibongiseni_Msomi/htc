<input type='hidden' name='_token' value='{{Session::token()}}'>

        <div class="input-field">
            <i class="mdi-social-person-outline prefix"></i>
            {!! Form::text('firstname', null, ['class'=>'form-control', 'id'=>'firstname']) !!}
            {!! Form::label('firstname', 'First Name :') !!}
        </div>
        <div class="input-field">
            <i class="mdi-social-person-outline prefix"></i>
            {!! Form::text('lastname', null, ['class'=>'form-control', 'id'=>'lastname']) !!}
            {!! Form::label('lastname', 'Last Name:') !!}
        </div>
        <div class="input-field">
            <i class="mdi-communication-call prefix"></i>
            {!! Form::text('contactnumber', null, ['class'=>'form-control', 'id'=>'contactnumber']) !!}
            {!! Form::label('contactnumber', 'Contact Number:') !!}
        </div>
        <div class="input-field">
            <i class="mdi-communication-email prefix"></i>
            {!! Form::text('email', null, ['class'=>'form-control', 'id'=>'email']) !!}
            {!! Form::label('email', 'E-Mail:') !!}
        </div>
        <div class="input-field">
            <i class="mdi-social-public prefix"></i>
            {!! Form::select('country_is', $countries, null, ['class'=>'form-control', 'id'=>'country_is']) !!}
            {!! Form::label('country_is', 'Country :') !!}
        </div>

        <div class="input-field">
            <i class="mdi-action-language prefix"></i>
            {!! Form::select('timezone_is',$timezones, null, ['class'=>'form-control', 'id'=>'timezone_is']) !!}
            {!! Form::label('timezone_is', 'Timezone :') !!}
        </div>

        <div class="input-field">
            <div class="col s12 m4 l4 offset-l4 offset-m4">
                {!! Form::submit($buttonText, ['class' => 'btn cyan waves-effect waves-light']) !!}
                <br><br>
            </div>
        </div>