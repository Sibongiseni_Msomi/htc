@extends('layouts.app')

@section('content')

@if(!\Auth::user()->hasRole('admin') && \Auth::user() != $user)
    <?php $user = \Auth::user(); ?>
@endif
    <div class="row">
        <div class="col s12 l10 m12 offset-l1">
            <div id="profile-page-header" class="card">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="{{ url('/' . 'images/user-profile-bg.jpg') }}" alt="user background">                    
                </div>
                <figure class="card-profile-image activator">
                       <span class="circle light-blue accent-4 darken-1 z-depth-2 white-text alphatar truncate" style="line-height: 110px; width:110px; font-size: 3rem;">{{ substr($user->firstname, 0, 1) }}</span>
                </figure>
                <div class="card-content">
                  <div class="row">
                    <div class="col s12 m4 l4 offset-m2 offset-l2">                        
                        <h4 class="card-title grey-text text-darken-4 truncate center-on-small-only">{{ $user->firstname }} {{ $user->lastname }}</h4>
                        <p class="medium-small grey-text center-on-small-only">{{ $user->email }}</p>                        
                    </div>
                    <div class="col s6 m3 l3">
                        <h4 class="card-title grey-text text-darken-3 center">Followers</h4>
                        <p class="medium-small grey-text center">{{ $user->number_of_followers }}</p>
                    </div>
                    <div class="col s6 m3 l3">
                        <h4 class="card-title grey-text center text-darken-3">Commission</h4>
                        <p class="medium-small grey-text center">{{ $commission }}</p>
                    </div>
                  </div>
                </div>
                <div class="card-reveal" style="display: none; transform: translateY(0px);">
                    <p>
                      <span class="card-title grey-text text-darken-4">{{ $user->firstname }} {{ $user->lastname }} <i class="mdi-navigation-close right"></i></span>
                    </p>

                    <p><i class="mdi-action-perm-phone-msg cyan-text text-darken-2"></i> {{ $user->contactnumber }}</p>
                    <p><i class="mdi-communication-email cyan-text text-darken-2"></i> {{ $user->email }}</p>
                    <p><i class="mdi-social-public cyan-text text-darken-2"></i> {{ $user->country_is }} </p>
                    <p><i class="mdi-action-language cyan-text text-darken-2"></i> {{ $user->timezone_is }}</p>
                    <div class="divider"></div>
                    <p><i class="mdi-mdi-notification-sync-problem
                     cyan-text text-darken-2"></i> <span class="grey-text">Last updated {{ $user->updated_at }}</span></p>
                </div>
            </div>
        </div>
    </div>
            
            
    <div class="row">
        <div class="col s12 l10 m12 offset-l1">
            <ul class="tabs tab-profile z-depth-1 light-blue" style="width: 100%;">
                <li class="tab col s3"><a class="white-text waves-effect waves-light active" href="#personalinfo"><i class="mdi-file-folder-shared"></i> Personal Info</a>
                </li>
                <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#package"><i class="mdi-content-archive"></i> Package</a>
                </li>
                <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#password"><i class="mdi-communication-vpn-key"></i> Password</a>
                </li>                      
            <div class="indicator" style="right: 471px; left: 0px;"></div></ul>
            <!-- Personal Info-->
            <div id="personalinfo" class="tab-content col s12 white z-depth-1" style="display: block;">
                <div class="row">
                <div class="col s10 offset-s1">
                    <h4 class="grey-text text-darken-3">
                        Personal Info
                        <br><br>
                    </h4>
                    {!! Form::model($user, ['method'=>'PATCH', 'url'=>'users/'.$user->id, 'class'=>'form-horizontal form-bordered']) !!}

                    @include('backend.users._form', ['buttonText'=>'Update changes'])
                    {!! Form::close() !!} <br><br>
                </div>
                </div>
            </div>
            <!-- Package -->
            <div id="package" class="tab-content col s12  white z-depth-1" style="display: none;">
                <div class="row">
                <div class="col s10 offset-s1">
                    <h4 class="grey-text text-darken-3">
                        Subscription Package
                        <br><br>
                    </h4>
                    {!! Form::model($user, ['method'=>'PATCH', 'url'=>'/users/subscription', 'role'=>'form', 'class'=>"form-horizontal form-bordered"]) !!}
                    <div class="input-field">
                        <i class="mdi-content-archive prefix"></i>
                        {!! Form::select('id',$packages, null, ['id'=>'package_is']) !!}
                        {!! Form::label('package_is', 'Package :', ['class'=>'' ]) !!}
                    </div>
                    <div class="input-field">
                        <div class="col s12 m4 l4 offset-l4 offset-m4">
                            <button type="submit" class="btn cyan waves-effect waves-light">Update Package</button>
                            <br><br>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                </div>
            </div>
            <!-- Password -->
            <div id="password" class="tab-content col s12 white z-depth-1" style="display: none;">
                <div class="row">
                <div class="col s10 offset-s1">
                    <h4 class="grey-text text-darken-3">
                        New Password <br><br>
                    </h4>
                    {!! Form::model($user, ['method'=>'PATCH', 'url'=>'/users/password', 'role'=>'form', 'class'=>"form-horizontal form-bordered"]) !!}

                        <div class="input-field">
                            <input id="password" type="password" class="validate" name="password">
                            <label for="password" class="">Password</label>
                        </div>
                        <div class="input-field">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                            <label for="password-confirm" class="">Confirm Password</label>
                        </div>
                        <div class="input-field">
                            <div class="col s12 m4 l4 offset-l4 offset-m4">
                                <button type="submit" class="btn cyan waves-effect waves-light">Update Password</button>
                                <br><br>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                </div>

            </div>
        </div>
    </div>
@endsection
