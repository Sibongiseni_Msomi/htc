@extends('layouts.app') @section('content')
@if($followersCollection->count()>0)
    <div class="row">
        <div class="col s12">
            <span class="hide">{{ $count = 0 }}</span>
            @foreach($followersCollection as $followers)
            <h3 class="title"></h3>
            <div class="card">
                <div class="card-content">
                    <span class="card-title">Level {{ $count+=1 }} </span>
                    <div class="divider"  style="margin-bottom: 10px"></div>
                    @foreach($followers as $follower)
                    <a href="{{ url('/users/' . $follower->id) }}" class="chip hoverable"> {{ $follower->firstname }} {{ substr($follower->lastname, 0, 1)}}. </a>
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>
    </div>
@else
@include('layouts.empty-content', ['message' => 'You don\'t have any followers yet.'])
@endif
@endsection