@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col s12 m6 l6 offset-l3 offset-m3">
            {!! Form::open(['url'=>'accounts', 'class'=>'form-horizontal form-bordered' ]) !!}
            @include('backend.accounts._form', ['buttonText'=>'Save'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection
