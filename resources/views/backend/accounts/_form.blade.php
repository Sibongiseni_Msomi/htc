<input type='hidden' name='_token' value='{{Session::token()}}'>
<div class="card">

    <div class="card-image" style="padding: 2rem;"><img src="/images/paypal.png" alt=""></div>
    <div class="card-content">
    @if(Auth::user()->country_is != 'South Africa')
        <div class="row">
            <div class="input-field col s12">
                {!! Form::hidden('bank_is', 'PayPal', ['id'=>'bank_is']) !!}
                {!! Form::hidden('code', '000000', ['id'=>'code']) !!}

                {!! Form::email('wallet', null, ['id'=>'wallet']) !!}
                {!! Form::label('wallet', 'PayPal Email Address:') !!}
                @if ($errors->has('wallet'))
                    <span class="help-block">
                        <strong>{{ $errors->first('wallet') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                {!! Form::text('owner', Auth::user()->firstname . ' ' . Auth::user()->lastname, ['id'=>'owner']) !!}
                {!! Form::label('owner', 'Name:') !!}
                @if ($errors->has('owner'))
                    <span class="help-block">
                        <strong>{{ $errors->first('owner') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    @else
        <div class="row">
            <div class="input-field col s12">
                <?php unset($banks['PayPal']); ?>
                {!! Form::select('bank_is', $banks, null , ['id'=>'bank_is']) !!}
                {!! Form::label('bank_is', 'Select Your Bank:') !!}
                @if ($errors->has('bank_is'))
                    <span class="help-block">
                        <strong>{{ $errors->first('bank_is') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                {!! Form::text('code', null, ['id'=>'code']) !!}
                {!! Form::label('code', 'Branch Code:') !!}
                @if ($errors->has('code'))
                    <span class="help-block">
                        <strong>{{ $errors->first('code') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                {!! Form::select('type_is', $types, null,['id'=>'type_is']) !!}
                {!! Form::label('type_is', 'Account Type:') !!}
                @if ($errors->has('type_is'))
                    <span class="help-block">
                        <strong>{{ $errors->first('type_is') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                {!! Form::text('owner', Auth::user()->firstname . ' ' . Auth::user()->lastname, ['id'=>'owner']) !!}
                {!! Form::label('owner', 'Account Holder:') !!}
                @if ($errors->has('owner'))
                    <span class="help-block">
                        <strong>{{ $errors->first('owner') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                {!! Form::text('wallet', null, ['id'=>'wallet']) !!}
                {!! Form::label('wallet', 'Account Number:') !!}
                @if ($errors->has('wallet'))
                    <span class="help-block">
                        <strong>{{ $errors->first('wallet') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    @endif
    </div>

    @if(isset($statuses))
        <div class="input-field">
           {!! Form::hidden('status_is', 'Active', ['id'=>'status_is']) !!}
        </div>
    @endif
    
    <div class="card-action">
        {!! Form::submit($buttonText, ['class' => 'btn green']) !!}
    </div>
</div>
