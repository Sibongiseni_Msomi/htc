@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col s12 m6 l6 offset-l3 offset-m3">
            {!! Form::model($account, ['method'=>'PATCH', 'url'=>'accounts/'.$account->id, 'class'=>'form-horizontal form-bordered']) !!}
            @include('backend.accounts._form', ['buttonText'=>'Update changes'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection
