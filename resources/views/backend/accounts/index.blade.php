@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <br>

            <form method = 'get' action = '{{url("accounts/create")}}'>
                <button class = 'btn btn-danger'>Add Account</button>
            </form>

            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="fa fa-table"></i>
                        List of Accounts
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <table class="table table-nomargin dataTable table-bordered">
                        <thead>
                        <th>Id</th>

                        <th>Bank</th>

                        <th>Type</th>

                        <th>Account</th>

                        <th>Status</th>

                        <th>Actions</th>
                        </thead>
                        <tbody>
                        @foreach($accounts as $account)
                            <tr>
                                <td data-label="Id">{{$account->id}}</td>

                                <td data-label="Method">{{$account->bank_is}}</td>

                                <td data-label="Wallet">{{$account->type_is}}</td>

                                <td data-label="account-number">{{$account->wallet}}</td>

                                <td data-label="Status">{{$account->status_is}}</td>

                                <td data-label="Actions">

                                    <a href = '{{url('/accounts/'.$account->id.'/edit')}}' class = 'viewEdit btn btn-primary btn-xs'><i class="mdi-editor-mode-edit" aria-hidden="true"></i></a>
                                    <a href="{{url('/accounts/'.$account->id)}}" class='viewShow btn btn-warning btn-xs'><i class="mdi-action-view-headline" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      </div>
@endsection
