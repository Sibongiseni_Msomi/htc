@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="page-header-actions">
                            <h4 class="page-title" style="padding-left:15px;">Show Account #{{$account->id}}</h4>
                            <ol class="breadcrumb">
                                <li><a href="{{ url('/') }}">Home</a></li>
                                <li><a href="{{ url('/accounts') }}">Accounts</a></li>
                                <li class="active">Show</li>
                            </ol>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <form method='get' action='{{url("accounts")}}'>
                                <button class='btn btn-primary'>Account Index</button>
                            </form>
                            <br>
                            <table class='table table-bordered'>
                                <thead>
                                <th>Heading</th>
                                <th>Value</th>
                                </thead>
                                @if(Auth::user()->country_is != 'South Africa')
                                <tbody>
                                <tr>
                                    <td>
                                        <b><i>PayPal ID : </i></b>
                                    </td>
                                    <td>{{$account->owner}}</td>
                                </tr>
                                <tr>
                                  <td>
                                      <b><i>Method : </i></b>
                                  </td>
                                  <td>{{$account->bank_is}}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><i>E-Mail Address : </i></b>
                                    </td>
                                    <td>{{$account->wallet}}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><i>Status : </i></b>
                                    </td>
                                    <td>{{$account->status_is}}</td>
                                </tr>
                                </tbody>

                              @else
                                <tbody>
                              <tr>
                                  <td>
                                      <b><i>Owner : </i></b>
                                  </td>
                                  <td>{{$account->owner}}</td>
                              </tr>
                              <tr>
                                <td>
                                    <b><i>Bank : </i></b>
                                </td>
                                <td>{{$account->bank_is}}</td>
                              </tr>
                              <tr>
                                  <td>
                                      <b><i>Account Number : </i></b>
                                  </td>
                                  <td>{{$account->wallet}}</td>
                              </tr>
                              <tr>
                                  <td>
                                      <b><i>Type : </i></b>
                                  </td>
                                  <td>{{$account->type_is}}</td>
                              </tr>
                              <tr>
                                  <td>
                                      <b><i>Status : </i></b>
                                  </td>
                                  <td>{{$account->status_is}}</td>
                              </tr>
                                </tbody>

                            @endif

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
