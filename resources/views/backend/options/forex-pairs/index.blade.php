@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s12 m6 l6 offset-m3 offset-l3">
            <div class="card">
                <div class="card-content">
            @include('errors.forms')
            {!! Form::open(['url'=>'settings/forex-pairs']) !!}
            @include('backend.options.forex-pairs._form', ['buttonText'=>'Save'])
            {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m6 l6 offset-m3 offset-l3">
            <div class="card">
                <div class="card-content">

                    List Pairs Here
                </div>
            </div>
        </div>
    </div>
</div>
@endsection