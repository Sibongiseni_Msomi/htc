<input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
<div class="row">
    <div class="col s9">
        <div class="input-field">
        {!! Form::text('option_value', null, ['class'=>'form-control', 'id'=>'option_value']) !!}
        {!! Form::label('option_value', 'Pair:', ['class'=>'control-label col-sm-2' ]) !!}
        </div>
    </div>
    <div class="col s3">
        <div class="input-field">
        {!! Form::submit($buttonText, ['class' => 'btn green darken-2']) !!}
        </div>
    </div>
</div>