@extends('layouts.app')

@section('content')
@if($withdrawals->count() > 0)
    <div class="row">
        <div class="col s12 m10 offset-m1 l10 offset-l1">
                <table class="table striped bordered z-depth-1 white responsive-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th># of Commissions</th>
                            <th>Pay-Day</th>
                            <th>Amount</th>
                            <th>Created</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($withdrawals as $withdrawal)
                            <tr>
                                <td>{{ $withdrawal->id }}</td>
                                <td>{{ count(unserialize($withdrawal->commissions)) }}</td>
                                <td>{{ $withdrawal->payday }}</td>
                                <td>{{ $withdrawal->amount }}</td>
                                <td>{{ $withdrawal->created_at->toFormattedDateString() }}</td>
                                @if($withdrawal->status_is == 'Paid')
                                <td> <span class="green white-text center badge2">{{ $withdrawal->status_is }}</span></td>
                                @elseif($withdrawal->status_is  == 'Processing')
                                <td> <span class="orange white-text center badge2">{{ $withdrawal->status_is }}</span></td>
                                @elseif($withdrawal->status_is  == 'Requested')
                                <td> <span class="blue white-text center badge2">{{ $withdrawal->status_is }}</span></td>
                                @elseif($withdrawal->status_is  == 'Declined')
                                <td> <span class="red white-text center badge2">{{ $withdrawal->status_is }}</span></td>
                                @endif
                                
                                <td>
                                <a href="{{ url('/withdrawals/'.$withdrawal->id ) }}" class='viewShow btn-flat black-text' style="padding: 0 10px;"><i class="mdi-action-visibility" aria-hidden="true"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            <div class="card">
                <div class="center white-text">
                    {{ $withdrawals->links() }}
                </div>
            </div>
        </div>
    </div>
@else

@include('layouts.empty-content')

@endif

@endsection