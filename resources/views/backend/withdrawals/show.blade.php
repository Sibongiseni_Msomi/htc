@extends('layouts.app')

@section('content')
@if(isset($withdrawal))
    <div class="row">
        <div class="col s12 m10 l10 offset-m1 offset-l1">
            <div id="flight-card" class="card">
                @if($withdrawal->status_is == 'Paid')
                <div class="card-header green darken-2">
                @elseif($withdrawal->status_is  == 'Processing')
                <div class="card-header amber darken-2">
                @elseif($withdrawal->status_is  == 'Requested')
                <div class="card-header blue darken-2">
                @elseif($withdrawal->status_is  == 'Declined')
                <div class="card-header red darken-2">
                @else
                <div class="card-header grey darken-2">
                @endif<br>
                    <div class="row">
                        <div class="col s8 m9 l10">
                            <div class="card-title">
                                <h4 class="flight-card-title">Amount: </strong>{{ $withdrawal->amount }}</h4> <br>
                                <div class="divider"></div>
                                <div class="row">
                                    <div class="col s12 m6">
                                <span class="grey-text text-lighten-3 small"><strong>Pay-Day:</strong> {{ $withdrawal->payday }} <br><br></span></div>
                                    <div class="col s12 m6">
                                    
                                <span class="grey-text text-lighten-3 small">Last Updated: {{ $withdrawal->updated_at->diffForHumans()}} </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s4 m3 l2">
                                <div class="center-align">
                                @if($withdrawal->status_is == 'Paid')
                                <i class="mdi-action-done-all medium white-text"></i>
                                @elseif($withdrawal->status_is  == 'Processing')
                                <i class="mdi-notification-sync-problem medium white-text"></i>
                                @elseif($withdrawal->status_is  == 'Requested')
                                <i class="mdi-action-done medium white-text"></i>
                                @elseif($withdrawal->status_is  == 'Declined')
                                <i class="mdi-navigation-close medium white-text"></i>
                                @endif<br>
                                <span class="flight-card-date">{{ $withdrawal->status_is }}</span>
                                </div>
                        </div>

                    </div>
                </div>
                <div class="card-content-bg white-text">
                    <div class="card-content">
                        <div class="row">
                            <div class="col s12 center-align">
                                    <h4 class="margin">Commissions</h4>
                            </div>
                        </div>
                        @if(isset($commissions))
                        <div class="row">
                            <div class="col s12">
                                <table class="responsive-table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Paid by</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($commissions as $commission)
                                        <tr>
                                            <td>{{ $commission->id }}</td>
                                            <td>{{ \App\User::where('id', $commission->from_user_id)->pluck('firstname')->first() }}</td>
                                            <td>{{ $commission->amount }}</td>
                                            <td>{{ $commission->created_at->toFormattedDateString() }}</td>
                                            <td>{{ $commission->status_is }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @else
                            @include('layouts.empty-content')
                        @endif
                    </div>
                </div>
            </div>
    </div><!-- col s12 m10 l10 offset-m1 offset-l1 -->
</div> <!-- row -->
@else
@include('layouts.empty-content')
@endif 
@endsection