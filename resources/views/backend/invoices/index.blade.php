@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12 red white-text">
                <h1 class="header">Summary
                </h1>
            </div>
            <table class="striped">
                <thead>
                    <tr>
                        <th># of Transactions</th>
                        <th># of Paid</th>
                        <th># of Pending</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td></td>
                        <td>Total: <strong>{{ $invoices->sum('amount') }}</strong></td>
                        <td>Due: <strong>{{ $totalDue }}</strong></td>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td>{{ $invoices->count() }} item(s)</td>
                        <td>{{ $invoices->where('status_is', 'Paid')->count() }}</td>
                        <td>{{ $pendingItems }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col s12 m12 l10 offset-l1">
            <h1>Invoices</h1>
                @foreach($invoices as $invoice)
                    @if($invoice->status_is == 'Paid')
                        <div class="card">
                            <div class="card-content">

                            <form method='get' action='{{url("invoices/$invoice->id")}}'>
                                <button type="submit" class="btn-flat waves-effect waves-teal right"> <i class="mdi-action-info-outline large"></i></button>
                            </form>
                                    <span class="card-title grey-text">Invoice #{{ $invoice->id }}</span>
                                    <p><strong>Amount </strong>{{ $invoice->amount }}</p>
                                    <p><strong>Status </strong>{{ $invoice->status_is}}</p>
                                    <p><strong>Transaction(s) </strong>{{ count(explode(',', $invoice->subscription_id)) }} linked</p>
                                <p><strong>Created </strong>{{ $invoice->created_at->toFormattedDateString() }} <span class="right grey-text">{{ $invoice->created_at->diffForHumans() }} </span></p>
                            </div>
                        </div>
                    @else
                        <div class="card">
                            <div class="card-content">
                            <form method='get' action='{{url("invoices/$invoice->id")}}'>
                                <button type="submit" class="btn-flat waves-effect waves-teal right"> <i class="mdi-action-info-outline large"></i></button>
                            </form>
                               <span class="card-title grey-text">Invoice #{{ $invoice->id }}</span>
                                <p><strong>Amount </strong>{{ $invoice->amount }}</p>
                                <p><strong>Status </strong>{{ $invoice->status_is}}</p>
                                <p><strong>Transaction(s) </strong>{{ count(explode(',', $invoice->subscription_id)) }} linked</p>
                                <p><strong>Created </strong>{{ $invoice->created_at->toFormattedDateString() }} <span class="right grey-text">{{ $invoice->created_at->diffForHumans() }} </span></p>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection
