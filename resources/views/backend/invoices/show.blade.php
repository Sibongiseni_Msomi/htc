@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12 m10 l10 offset-l1 offset-m1 card ">
                <div class="card-content">
                        <span class="card-title grey-text">Invoice #{{ $invoice->id }}</span>
                        <p><strong>Amount </strong>{{ $invoice->amount }}</p>
                        <p><strong>Status </strong>{{ $invoice->status_is}}</p>
                    <p><strong>Created </strong>{{ $invoice->created_at->toFormattedDateString() }} <span class="right grey-text">{{ $invoice->created_at->diffForHumans() }} </span></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m10 l10 offset-l1 offset-m1 red white-text">
                <h1 class="header">Transactions
                </h1>
            </div>
            <div class="col s12 m10 l10 offset-l1 offset-m1">
                <table class="table striped">
                <thead>
                    <tr>
                        <th>Transaction ID</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Type</th>
                    </tr>
                </thead>
                    <tbody>

                    @if($commission != null)
                        @foreach($commission as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->amount }}</td>
                            <td>{{ $item->created_at->toFormattedDateString() }}</td>
                            <td>{{ $item->status_is }}</td>
                            <td>Commission</td>
                        </tr>
                        @endforeach
                    @elseif($subscription != null)
                        @foreach($subscription as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->package->amount }}</td>
                            <td>{{ $item->created_at->toFormattedDateString() }}</td>
                            <td>{{ $item->status_is }}</td>
                            <td>Subscription</td>
                        </tr>
                        @endforeach

                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection