@extends('layouts.app')

@section('content')

<div class="container"></div>
    <div class="row">
        <div class="col s12 m10 l10 offset-l1 offset-m1">
        <div class="row">
            <div class="col s12 m12 l4">
                <div class="card white-text blue">
                    <div class="card-content white-text">
                        <p class="card-stats-title"><i class="mdi-action-done blue white-text"></i> Requested</p>
                        <h4 class="card-stats-number">{{ number_format($paymentsMeta['tRequested'], 2, '.', '') }}</h4>
                    </div>
                    <div class="card-action blue darken-3 white-text">
                        <div class="row">
                            <div class="col s12 m6 l6"><span class="left">{{ $paymentsMeta['cRequested'] }} {{ ($paymentsMeta['cRequested'] > 1 ? 'Transactions' : 'Transaction') }}</span></div>
                            <div class="col s12 m6 l6"><a href="#!" class="right ultra-small waves-effect waves-light"><i class="mdi-action-query-builder"></i> Process All</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m12 l4">
                <div class="card white-text orange">
                    <div class="card-content white-text">
                        <p class="card-stats-title"><i class="mdi-action-query-builder orange white-text"></i> Processing</p>
                        <h4 class="card-stats-number">{{ number_format($paymentsMeta['tProcessing'], 2, '.', '') }}</h4>
                    </div>
                    <div class="card-action orange darken-3 white-text">
                        <div class="row">
                            <div class="col s12 m6 l6"><span class="left">{{ $paymentsMeta['cProcessing'] }} {{ ($paymentsMeta['cProcessing'] > 1 ? 'Transactions' : 'Transaction') }}</span></div>
                            <div class="col s12 m6 l6"><a href="#!" class="right ultra-small waves-effect waves-light"><i class="mdi-action-done-all"></i> Complete All</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m12 l4">
                <div class="card white-text green">
                    <div class="card-content white-text">
                        <p class="card-stats-title"><i class="mdi-action-done-all green white-text"></i> Paid</p>
                        <h4 class="card-stats-number">{{ number_format($paymentsMeta['tPaid'], 2, '.', '') }}</h4>
                    </div>
                    <div class="card-action green darken-3 white-text">
                        <div class="row">
                            <div class="col s12 m6 l6"><span class="left">{{ $paymentsMeta['cPaid'] }} {{ ($paymentsMeta['cPaid'] > 1 ? 'Transactions' : 'Transaction') }}</span></div>
                            <div class="col s12 m6 l6"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <div class="card-panel">  
                    <table class="responsive-table white striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Paypal ID</th>
                                <th>Amount</th>
                                <th>Currency</th>
                                <th>Country</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($paymentsCollection as $payment)

                            <tr>
                                <td>{{ $payment['id'] }}</td>
                                <td>{{ $payment['firstname'] }}</td>
                                <td>{{ $payment['paypal_id'] }}</td>
                                <td>{{ $payment['amount'] }}</td>
                                <td>{{ $payment['currency'] }}</td>
                                <td>{{ $payment['country'] }}</td>                                
                                @if($payment['status'] == 'Paid')
                                <td> <span class="green white-text center badge2">{{ $payment['status'] }}</span></td>
                                @elseif($payment['status']  == 'Processing')
                                <td> <span class="orange white-text center badge2">{{ $payment['status'] }}</span></td>
                                @elseif($payment['status']  == 'Requested')
                                <td> <span class="blue white-text center badge2">{{ $payment['status'] }}</span></td>
                                @elseif($payment['status']  == 'Declined')
                                <td> <span class="red white-text center badge2">{{ $payment['status'] }}</span></td>
                                @endif
                            </tr>

                        @endforeach
                    </tbody>
                </table>                
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

@endsection