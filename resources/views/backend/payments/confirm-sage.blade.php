@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col s12 m4 l4 offset-m4 offset-l4">
            <div class="card">
                <div class="card-image"></div>
                <div class="card-content">Please press pay below to be redirected to form</div>
                <div class="card-action">
                    <form class="form-horizontal" method="POST" id="SagePayForm" action="https://pi-test.sagepay.com/api/v1/transactions"> 
                        {{ csrf_field() }}
                        
                        <input type="hidden" name="VPSProtocol" value= "3.00">
                        <input type="hidden" name="TxType" value= "PAYMENT">
                        <input type="hidden" name="Vendor" value= "sandbox">
                        <input type="hidden" name="Crypt" value= "{{ $encrypted_code }}">

                        <button type="submit" class="btn">
                            Pay
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection