@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col m8 offset-m2 s12">
                <div class="card-panel">
                    <h4 class="header">Amount <strong>{{ $amount }}</strong></h4>
                    <div class="card-content">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('payments/make') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('method_is') ? ' has-error' : '' }}">
                                <label for="method_is">Choose Method</label>

                                <div class="col-md-6">
                                    {!! Form::select('method_is', $methods, null, ['class'=>'form-control', 'id'=>'method_is']) !!}

                                    @if ($errors->has('method_is'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('method_is') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-credit-card"></i> Proceed
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop