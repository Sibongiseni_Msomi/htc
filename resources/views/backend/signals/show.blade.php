@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="page-header-actions">
                            <h4 class="page-title" style="padding-left:15px;">Show invoice item #{{$signal->id}}</h4>
                            <ol class="breadcrumb">
                                <li><a href="{{ url('/') }}">Home</a></li>
                                <li><a href="{{ url('/signals') }}">Signals</a></li>
                                <li class="active">Show</li>
                            </ol>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <form method='get' action='{{url("signals")}}'>
                                <button class='btn btn-primary'>Signals Index</button>
                            </form>
                            <br>
                            <table class='table table-bordered'>
                                <thead>
                                <th>Heading</th>
                                <th>Value</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <b><i>Id : </i></b>
                                    </td>
                                    <td>{{ $signal->id }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><i>Short Message : </i></b>
                                    </td>
                                    <td>{{ $signal->short_message }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><i>Currency Pair : </i></b>
                                    </td>
                                    <td>{{ $signal->pair_is }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><i>Direction : </i></b>
                                    </td>
                                    <td>{{ $signal->direction_is }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><i>Action : </i></b>
                                    </td>
                                    <td>{{ $signal->action_is }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><i>Entry : </i></b>
                                    </td>
                                    <td>{{ $signal->entry_is }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><i>Stop Loss : </i></b>
                                    </td>
                                    <td>{{ $signal->stoploss_is }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><i>Take Profit : </i></b>
                                    </td>
                                    <td>{{ $signal->takeprofit_is }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><i>Status : </i></b>
                                    </td>
                                    <td>{{ $signal->status_is }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><i>Created at : </i></b>
                                    </td>
                                    <td>{{ $signal->created_at->toFormattedDateString() }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection