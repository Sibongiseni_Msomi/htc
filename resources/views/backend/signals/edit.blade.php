@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col l10 offset-s2">
                <div class="card">
                    <div class="card-content">
                        <div class="col l12">
                            <form method = 'get' action = '{{url("signals")}}'>
                                <button class = 'btn red waves-effect waves-dark'>Signal List</button>
                            </form>
                            <br>
                            {!! Form::model($signal, ['method'=>'PATCH', 'url'=>'signals/'.$signal->id]) !!}
                                @include('backend.signals._form', ['buttonText'=>'Update changes', 'class' => 'red'])
                            {!! Form::close() !!}

                            @include('errors.forms')
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection