@extends('layouts.app')

@section('content')

    @if(Session::has('submittedSignal'))
        <div id="submittedSignal" hidden>{{\Illuminate\Support\Facades\Session::get('submittedSignal')}}</div>
        <div id="eligibleUsers" hidden>{{\Illuminate\Support\Facades\Session::get('eligibleUsers')}}</div>
        <script>
            window.onload = setInterval(sendSubmittedSignal(), 3000);
        </script>
        {{Session::forget('submittedSignal')}}
        {{Session::forget('eligibleUsers')}}
    @endif

    <div class="row">
        <div class="col s12 m12 l12">
        @if($user->hasRole('admin'))
            <p><a href="{{ url('/signals/create') }}" class="btn teal waves-effect waves-light">Create Signal</a></p>
        @endif
        @if($signals->count()>0)
                <ul class="collection z-depth-1">
                    @foreach($signals as $signal)
                    <li class="collection-item avatar">
                        <div class="row">
                            <div class="col s6  blue-grey-text">
                                @if($signal->action_is ==  'buy')
                                    <i class="mdi-action-trending-up circle green"></i>
                                @else
                                    <i class="mdi-action-trending-down circle red"></i>
                                @endif
                                <span class="collection-header"><strong>{{ ucwords($signal->action_is) }} {{ strtoupper($signal->pair_is) }}  {{ $signal->entry_is}}</strong></span>
                                <p>
                                    <strong>SL </strong>{{$signal->stoploss_is}} <br> <strong>TP</strong> {{$signal->takeprofit_is}} <br> ID {{ $signal->id }}
                                </p>
                            </div>
                            <div class="col s6 right-align">
                            @if($user->hasRole('admin'))
                                {!! Btn::delete($signal->id) !!}
                                <a href="{{url('/signals/'.$signal->id.'/edit')}}" class="btn-flat waves-effect waves-dark" style="padding: 0 10px;"><i class="mdi-content-create"></i></a>
                                <a onclick="broadCastSignal({{$signal->id}})" id="{{$signal->id}}" class="btn-flat waves-effect waves-dark" style="padding: 0 10px;"><i class="mdi-content-send"></i></a>
                            @endif
                            <p class="right-align blue-grey-text"> <br> <i class="mdi-action-schedule"></i> {{ $signal->created_at->diffForHumans() }}</p>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        @else
        @include('layouts.empty-content', ['message' => 'Nothing to display!'])
        @endif
        </div>
    </div>
    <div class="row">
        <div class="col s12">
                <div class="center grey-text text-darken-4">
                    {{ $signals->links() }}
                </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

        function sendSubmittedSignal() {
            console.log('submitted');
            var data = {"signal": $('#submittedSignal').text(), "eligibleUsers": $('#eligibleUsers').text()};
            var socket = io.connect("http://35.162.236.82:3000");
            socket.emit('send-new-signal', data);
        }

        function broadCastSignal(id) {

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var url = window.location.href;     // Returns full URL
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': CSRF_TOKEN}
            });

            var newString = url.substr(0, url.length - 7);

            console.log('broadCastSignal');

            var newUrl = newString + 'push-signal';

            $.ajax({
                type: 'POST',
                url: newUrl,
                data: {id: id},
                dataType: 'json',
                success: function (data) {
                    // var overLay = $("#siteOverlay");
                    console.log(data);
                    console.log(io);
                    var socket = io.connect("http://35.162.236.82:3000");
                    socket.emit('send-new-signal', data);
                    // openNav();
                }, error: function (xhr, textStatus, errorThrown) {
                    console.log(xhr);
                    alert(xhr);
                }
            });
        }
    </script>
@endsection
