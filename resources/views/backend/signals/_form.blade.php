<input type = 'hidden' name = '_token' value = '{{Session::token()}}'>

<div class="box-body">
	<div class="row">
		<div class="col s12">
			{!! Form::label('pair_is', 'Pairs:') !!}
			{!! Form::select('pair_is', $pairs, null, ['class'=>'form-control', 'id'=>'pair_is']) !!}
		</div>
	</div>
	<div class="row">
		<div class="col s6">
			{!! Form::label('direction_is', 'Direction:') !!}
			{!! Form::select('direction_is', $direction, null, ['class'=>'form-control', 'id'=>'direction_is']) !!}
		</div>
		<div class="col s6">
			{!! Form::label('action_is', 'Action:') !!}
			{!! Form::select('action_is', $action, null, ['class'=>'form-control', 'id'=>'action_is']) !!}
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			{!! Form::label('entry_is', 'Entry:') !!}
			{!! Form::text('entry_is', null, ['class'=>'form-control', 'id'=>'entry_is']) !!}
		</div>
	</div>
	<div class="row">
		<div class="col s6">
			{!! Form::label('takeprofit_is', 'Take Profit:') !!}
			{!! Form::text('takeprofit_is', null, ['class'=>'form-control', 'id'=>'takeprofit_is']) !!}
		</div>
		<div class="col s6">
			{!! Form::label('stoploss_is', 'Stop Loss:') !!}
			{!! Form::text('stoploss_is', null, ['class'=>'form-control', 'id'=>'stoploss_is']) !!}
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			{!! Form::label('status_is', 'Status:') !!}
			{!! Form::select('status_is', $statuses, null, ['class'=>'form-control', 'id'=>'status_is']) !!}
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			{!! Form::label('short_message', 'Short Message:') !!}
			{!! Form::text('short_message', null, ['class'=>'form-control', 'id'=>'short_message']) !!}
		</div>
	</div>
</div>
<div class="box-footer">
	{!! Form::submit($buttonText, ['class' => 'btn btn-primary']) !!}
</div>