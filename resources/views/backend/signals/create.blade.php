@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col m10 offset-s2">
                <div class="card">
                    <div class="card-content">
                            <form method = 'get' action = '{{url("signals")}}'>
                                <button class = 'btn btn-danger'>Signal Index</button>
                            </form>
                            <br>
                            {!! Form::open(['url'=>'signals']) !!}
                            @include('backend.signals._form', ['buttonText'=>'Save'])
                            {!! Form::close() !!}

                            @include('errors.forms')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection