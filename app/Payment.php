<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'amount',
    	'user_id',
    	'follower_is',
        'journal_entry',
    	'status_is'
    ];

    /**
     * The array of $statuses.
     *
     * @var array
     */
    public static $statuses = [
    	'inprogress' => 'In Progress',
    	'processing' => 'Processing',
    	'processed' => 'Processed',
    	'successful' => 'Successful',
    	'failed' => 'Failed'
    ];


    public static $methods = [
    	'EFT' => 'EFT',
    	'PayPal' => 'PayPal',
        'Credit' => 'Credit/Debit Card',
    ];
    
    public static $journalEntries = [
        'debit' => 'Debit',
        'credit' => 'Credit'
    ];


    //Each Payment belongsTo one User
    public function user(){
    	return $this->belongsTo('App\User');
    }
}
