<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'user_id',
        'bank_is',
        'status_is',
        'type_is',
        'owner',
        'account_number',
        'branch_code',
        'wallet',
        'code',
        'swift_code',
    ];

    public static $banks = [
      'FNB' => 'FNB',
      'Capitec' => 'Capitec',
      'ABSA' => 'ABSA',
      'Standard Bank' => 'Standard Bank',
      'Nedbank' => 'Nedbank',
      'PayPal' => 'PayPal',
    ];

    public static $statuses = [
        'Inactive' => 'Inactive',
        'Active' => 'Active',
        'Blocked' => 'Blocked',
    ];

    public static $types = [
        'Savings' => 'Savings',
        'Cheque' => 'Cheque',
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
