<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model
{
    // "countryCode": "AD",
    // "countryName": "Andorra",
    // "zoneName": "Europe\/Andorra",
    // "gmtOffset": 3600,
    // "timestamp": 1483437751  
    protected $fillable = [
        'countryCode', 
        'countryName',
        'zoneName', 
        'gmtOffset', 
        'timestamp'
    ];
}
