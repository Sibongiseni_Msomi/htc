<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    //
     	protected $fillable = [
		'from_user_id',
		'to_user_id',
		'amount',
		'status_is'
	];

 	public static $statuses = [
		'Pending' => 'Pending',
		'Paid' => 'Paid',
        'Unpaid' => 'Unpaid'
 	];

 	public function user(){
 		return $this->belongsTo('App\User');
 	}
}
