<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('layouts.site');
});

Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('follow/me/{userId}', ['as' => 'follow_me', 'uses' => 'SiteController@follow']);
Route::post('/withdraw',['as'=> 'withdraw', 'uses' => 'HomeController@withdraw']);

// Verification function of HomeController
Route::post('/verification', 'HomeController@verification');

// Resend code function of HomeController
Route::get('/resend', 'HomeController@resend');


//resource to followers routes
Route::resource('followers', 'FollowersController');

//Resource to signal items routes
Route::resource('signals', 'SignalsController');


//***************Payments BY clients*****************
Route::get('/payments/make', 'PaymentsController@make');
Route::get('/payments/finalize', 'PaymentsController@finalize');
Route::post('/payments/make', 'PaymentsController@process');

//***************Payments TO clients*****************
Route::get('/payments/paypal-payments', ['as' => 'paypal-payments', 'uses' => 'PaymentsController@paypal_payments']);
Route::get('/payments/process_paypal_clients', ['as' => 'process-paypal-clients', 'uses' => 'PaymentsController@process_paypal_clients']);
Route::get('/payments/sagepay-payments', ['as' => 'sagepay-payments', 'uses' => 'PaymentsController@sagepay_payments']);

//Resource to payment items routes
Route::resource('payments', 'PaymentsController');

//Resource to Invoices items route
Route::resource('invoices', 'InvoicesController');

//Resource to Subscription
Route::resource('subscriptions', 'SubscriptionsController');

//Resource to Withdrawal
Route::resource('withdrawals', 'WithdrawalController');

//Ajax Methods
Route::post('/direct-referrals', 'AjaxController@directReferralsData');
Route::post('/push-signal', 'AjaxController@pushSignal');

Route::resource('accounts', 'AccountsController');

Route::patch('/users/subscription', 'UsersController@subscription');
Route::patch('/users/password', 'UsersController@password');
Route::resource('users','UsersController');

//Route to get timezones as countries from API and store to DB
Route::get('/timezone/get', ['as' => 'get_timezones', 'uses' => 'SiteController@gettimezones']);


//Resource to Options/"Forex Pairs" items routes
Route::resource('settings/forex-pairs', 'ForexPairsOptionsController');
