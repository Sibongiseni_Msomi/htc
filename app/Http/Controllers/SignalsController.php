<?php

namespace App\Http\Controllers;

use App\Signal;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SignalFormRequest;

class SignalsController extends Controller
{
    public $breadcrumbs = array('page'=>'Signals', 'single'=>'Signals', 'current'=>'Index', 'header'=>'', 'back'=>'signals');
    public function __construct()
    {
        $this->middleware(['auth']);
        $this->middleware('admin' , ['except' => 'index']);
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page = $this->breadcrumbs;
        // Get all signals
        $signals = Signal::orderby('created_at','desc')->paginate(10);
        $user = Auth::user();
        return view('backend.signals.index', compact('signals', 'user', 'page'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page = $this->breadcrumbs;
        // Create new signal
        $pairs = Signal::$pairs;
        $direction = Signal::$direction;
        $action = Signal::$action;
        $statuses = Signal::$statuses;
        return view('backend.signals.create', compact('pairs', 'direction', 'action', 'statuses', 'page'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(SignalFormRequest $request)
    {
        // Save the new signal
        Signal::create($request->all());
        flash('New signal has been added!', 'green');
        return redirect('signals');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show(Signal $signal)
    {
        $page = $this->breadcrumbs;
        // Show signal of $id
        return view('backend.signals.show', compact('signal', 'page'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit(Signal $signal)
    {
        $page = $this->breadcrumbs;
        // Edit existing signal
        $pairs = Signal::$pairs;
        $direction = Signal::$direction;
        $action = Signal::$action;
        $statuses = Signal::$statuses;
        return view('backend.signals.edit', compact('signal', 'pairs', 'direction', 'action', 'statuses', 'page'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(SignalFormRequest $request, Signal $signal)
    {
        // Update the existing signal
        $signal->update($request->all());
        flash('Signal has been updated!','green');
        return redirect('signals');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Signal $signal)
    {
        // Delete a signal
        $signal->delete();
        flash('Signal has been deleted!', 'green');
        return redirect('signals');
    }

    /**
     * Send resource to all users via email and sms.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function send(Signal $signal)
    {
        $users = User::where('status_is', 'Active')->get();
        $eligibleUsers = [];
        foreach ($users as $user) {
            if (($user->number_of_signals > 0 && $user->number_of_signals <= 20) || $user->number_of_signals == 100) {
                if ($user->signals != "") {
                    $usersSignals = explode(',', $user->signals);
                    array_push($usersSignals, $signal->id);
                    $user->signals = implode(',', $usersSignals);
                    $user->save();
                } else {
                    $user->signals = $signal->id;
                    $user->save();
                }

                if ($user->number_of_signals > 0 && $user->number_of_signals <= 20) {
                    $user->update(['number_of_signals' => $user->number_of_signals - 1]);
                }
                array_push($eligibleUsers, $user->id);
            }
        }

        Session::set('eligibleUsers', json_encode($eligibleUsers));
        Session::set('submittedSignal', json_encode($signal));

        //Send a signal
        $smsMessage=$signal->action_is.' at Market price. Entry: '.
            $signal->entry_is.', TP: '.
            $signal->takeprofit_is.' SL:'.
            $signal->stoploss_is;

        $emailMessage=$signal->action_is.' at Market price. <b>Entry point:</b> '.
            $signal->entry_is.', <b>Take Profit:</b> '.
            $signal->takeprofit_is.' <b>Stop Loss:</b> '.
            $signal->stoploss_is;

        // Get all users
        $users = User::all();
        $digitsArray=array();

        // Send via email and sms
        foreach ($users as $user) {
            $email = $user->email;
            $mobile = $user->mobile;
            $name = $user->username;
            $intro = $signal->short_message;
            $parameters = ['name'=>$name, 'intro'=>$intro, 'emailMessage'=>$emailMessage];

            // Create cellphone number array
            $digits='27'.substr($mobile,1);
            array_push($digitsArray,$digits);

            // Send email
//            Mail::queueOn('capsinvest','backend.emails.signal', $parameters, function ($message)
//            use ($email, $name) {
//                $message->to($email, $name)->subject('ALERT: TCH signal');
//            });
        };

        // Send SMS
//        $result=Sms::send($digitsArray,'sms.signal',['content'=>$smsMessage])->response();

        flash('signal has been sent!', 'green');
        return redirect('signals');
    }
}
