<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\User;

use App\Http\Requests;

class FollowersController extends Controller
{
    
    public $breadcrumbs = array('page'=>'Followers', 'single'=>'Follower', 'current'=>'Index', 'header'=>'', 'back'=>'followers');
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $page=$this->breadcrumbs;
        $user = Auth::user();//User::where('id', 41)->first();//
        

        $followers = User::where('referral_id', $user->id)->get();
        
        $followersCollection = collect([]);
        $haveFollowers = false;

        if(!$followers->isEmpty()){
            $followersCollection->push($followers);
            $haveFollowers = true;
        }

        $count =0;
        while($haveFollowers AND $count < 6){
            
            $followers = $this->getNextFollowers($followers, $count);
            if(!$followers->isEmpty()){
                $followersCollection->push($followers);
            }else{
                $haveFollowers = false;
            }
            $count+=1;
        }
        return view('backend.followers.index', compact('followersCollection', 'page'));
    }


    public function getNextFollowers($followers, $count)
    {
        if(!$followers->isEmpty()){
            $mergeCollection = collect([]);

            foreach ($followers as $follower) {
                $current = User::where('referral_id', $follower->id)->get();
                if(!$current->isEmpty()){
                    $mergeCollection = $mergeCollection->merge($current);
                    $count++;
                }
            }
            return $mergeCollection;
        }
        return collect([]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
