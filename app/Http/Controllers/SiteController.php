<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Package;
use App\User;
use App\Timezone;
use App\Http\Requests;

use Session;

class SiteController extends Controller
{
    
    /**
     * Follow user.
     *
     * @return \Illuminate\Http\Response
     */
    public function follow($userId){

        if ($userId){
            $user = User::where('id', $userId)->first();
            if($user != null)
            if ($user->verified) {
                Session::put('followed_user', $user);
                $countries = User::$countries;
                $timezones = User::$timezones;
                $packages = Package::get()->lists('name','id',  'features', 'amount');
                return view('auth.register', compact('countries','timezones', 'packages'));
            }
        }
        return redirect('/');
    }

    /**
    * Get Timezones from Timezonedb API
    *
    * @return \Illuminate\Http\Response
    */
    public function gettimezones()
    {
        $response = json_decode(file_get_contents(env('TIMEZONE_API')),true);
        
        if($response['status'] == "OK"){
            Timezone::truncate();//reset the table first
            foreach ($response['zones'] as $zone) {
                Timezone::create($zone);
            }
            return response()->json(['message' => 'Data updated!']);
        }
        return response()->json([
                                'Status' => $response['status'], 
                                'message' => $response['message']]);
    }
}
