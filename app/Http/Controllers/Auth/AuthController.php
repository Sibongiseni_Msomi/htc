<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Package;
use App\Invoice;
use App\Subscription;
use App\Timezone;
use Session;

use Carbon\Carbon;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }



    public function showRegistrationForm()
    {
        $countries = Timezone::pluck('countryName','countryName');
        $timezones = Timezone::pluck('zoneName', 'zoneName');
        $packages = Package::get()->lists('name','id',  'features', 'amount');
        return view('auth.register', compact('countries','timezones', 'packages'));
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'contactnumber' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
       $user = User::create([
           'firstname' => $data['firstname'],
           'lastname' => $data['lastname'],
           'contactnumber' => $data['contactnumber'],
           'number_of_followers' => 0,
           'number_of_signals' => 0,
           'code' => str_random(6), //Generate 6 Chars Verification code
           'verified' => false, //User not verified initially: TODO: Send Code Via Email
           'status_is' => 'Active', //User not active initially: TODO: Activated when Paid Successfully
           'country_is' => $data['country_is'],
           'timezone_is' => $data['timezone_is'],
           'timezone_offset' => Timezone::where('zoneName', $data['timezone_is'])->pluck('gmtOffset')->first(),
           'email' => $data['email'],
           'password' => bcrypt($data['password']),
       ]);

       // Do Spillover
        if(Session::has('followed_user')){
            # domain/follow/me/{$id}
            $this->doSpillover(Session::get('followed_user'), $user);
            Session::forget('foundUser');
        }else {
            # domain/register
            # not effecient but, working my way down the tree from the top to see if I can find an available space.
            $firstUser = User::where('created_at', User::min('created_at'))->get()->first();

            //dd($firstUser);
            $this->doSpillover($firstUser, $user);
        }
            
        //getPackage
        $package = Package::findOrFail($data['package']);

        //Add subscription
        $subscription = Subscription::create([
            'start_date' => (new Carbon('now'))->toDateString(),
            'expiry_date' => (new Carbon('now'))->addMonths(1)->toDateString(), // Not yet verified
            'user_id' => $user->id,
            'package_id' => $package->id,
            'status_is' => 'Pending',
        ]);
        // Assign Role to User 'member'
            $user->actAs('Member');
        //Add invoice
        $invoice = $this->createUserInvoice($user, $subscription, $package->amount);

        return $user;
      }

      /**
      * Function Create User Invoice
      * @param $user
      * @param $subscription
      * @param $amount
      * @return App\Invoice
      */
      public function createUserInvoice(User $user, Subscription $subscription, $amount)
      {
        // Create & Return Invoice
        return Invoice::create([
            'user_id' => $user->id,
            'subscription_id' => $subscription->id,
            'status_is' => 'Pending',
            'amount' => $amount,
        ]);
      }

      /**
      * Function doSpillover, performs the spillover, for both follow/me/{$id} and /register links
      *
      *
      */
      public function doSpillover($startUser, $user)
      {
          # code...
            $followed_user =  $startUser;
            $followers = User::where('referral_id', $followed_user->id)->get();
            $result = null;
            #Check if the followed user has followers and that his followers are less than 3 (MAX)
            if($followers !==null && $followers->count() >= 3){
                $result = $this->findSpillUser($followers, false);
                while ($result !== null) {
                    $result = $this->findSpillUser($result, false);
                }
            }elseif ($followers->count() < 3) {
                Session::put('foundUser', $followed_user);
            }

            #Spillover to Session foundUser
            $spilloveruser = Session::get('foundUser');
            $user->update(['position' => User::where('referral_id', $spilloveruser->id)->count()+1, 'referral_id' => $spilloveruser->id]);
            $spilloveruser->update(['number_of_followers' => $spilloveruser->number_of_followers + 1]);
      }


    /**
    * Function findSpillUser
    *
    * @param Collection $followersList 
    * @param Boolean $foundFlag
    * @return Collection $nextList : null
    */
    public function findSpillUser($followersList, $foundFlag)
    {
        $foundUser = null;
        $mergeList = collect([]);
        
        foreach ($followersList as $singleFollower) {
            
            $followers = User::where('referral_id', $singleFollower->id)->get();
            $followersCount = $followers->count();

            if($followersCount < 3){
                # I found someone, no need to continue, I'll just save him and tell the loops to stop.
                $foundFlag = true;
                $foundUser = $singleFollower;
                Session::put('foundUser', $foundUser);
                return null; #stops both foreach and while loops
            }
            else {
                # merging the little tiny incoming collections for this call so i can return one big collection, 
                # incase I dont find anyone available
                $mergeList = $mergeList->merge($followers); 
            }
        }

        if(!$foundFlag){
            # I haven't found anyone, I'll check the nextList of people
           return $mergeList;
        }else {
            # I found someone
            Session::put('foundUser', $foundUser);
            return null;
        }
    }

}
