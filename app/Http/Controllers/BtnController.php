<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class BtnController extends Controller
{
    // Delete function
    public static function delete($id, $name=null) {
        return view('layouts.delete', compact('id', 'name'))->render();
    }
}
