<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use App\Commission;
use App\Invoice;
use App\Subscription;
use App\User;

use App\Http\Requests;

class InvoicesController extends Controller
{

    public $breadcrumbs = array('page'=>'Invoice', 'single'=>'Invoice', 'current'=>'Index', 'header'=>'', 'back'=>'invoices');
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //TODO: Check if user is in admin role, return display all if true : ? display for Auth::user()
        $page = $this->breadcrumbs;

        $user = Auth::user();
        if($user->hasRole("admin")){
            $invoices = Invoice::get();
            $totalDue = Invoice::whereNotIn('status_is', ['Paid'])->sum('amount');
            $pendingItems = Invoice::whereNotIn('status_is', ['Paid'])->count();
        }else{
            $invoices = Invoice::where('user_id', $user->id)->get();
            $totalDue = Invoice::where('user_id', $user->id)->whereNotIn('status_is', ['Paid'])->sum('amount');
            $pendingItems = Invoice::where('user_id', $user->id)->whereNotIn('status_is', ['Paid'])->count();
        }
        
        return view('backend.invoices.index', compact('invoices', 'totalDue', 'pendingItems', 'page'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $page = $this->breadcrumbs;
         $page['current'] = "#".$id;
        
        $invoice=Invoice::where('id',$id)->first();
        $sourceIds = explode(',', $invoice->subscription_id);
        
        $commission = null;
        $subscription = null;

        if(count($sourceIds) > 1){
            $commission = Commission::whereIn('from_user_id', $sourceIds)->get();
        }else{
            $subscription = Subscription::whereIn('id', $sourceIds)->get();
        }
        
        return view('backend.invoices.show', compact('invoice', 'commission','subscription', 'page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
