<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Option;
use App\Http\Requests;

class ForexPairsOptionsController extends Controller
{
   public $breadcrumbs = array('page'=>'Settings', 'single'=>'Settings', 'current'=>'Index', 'header'=>'', 'back'=>'home');
    
    public function __construct()
    {
        $this->middleware('auth'); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page=$this->breadcrumbs;
        //
        $forexpairs = Option::where('option_name','forex-pairs')->pluck('option_value');
        //$forexpairs = unserialize($forexpairs);

        //dd($forexpairs);

        return view('backend.options.forex-pairs.index', compact('page','forexpairs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
