<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use SagePay;

use Illuminate\Http\Request;

use Omnipay\Common\CreditCard;
use Omnipay\Omnipay;

use App\Commission;
use App\Invoice;
use App\Level;
use App\Payment;
use App\Signal;
use App\Subscription;
use App\User;
use App\Withdrawal;
use App\Http\Requests;

class PaymentsController extends Controller
{

    public $breadcrumbs = array('page'=>'Payments', 'single'=>'Payments', 'current'=>'Index', 'header'=>'', 'back'=>'payments');
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = $this->breadcrumbs;
        $encrypted_code = SagePay::getCrypt();
        return view('backend.payments.confirm-sage', compact('encrypted_code', 'page'));
    }


    public $requestedPayPalIds = [];
    public $processingPayPalIds = [];

    public function paypal_payments()
    {
        $page = $this->breadcrumbs;
        $page['page'] = 'Payments';
        $page['current'] = 'Paypal Payments';
        $payments = Withdrawal::get();
        $paymentsCollection = collect([]);
        $paymentsMeta = [
            'tRequested' => 0,
            'tProcessing' => 0,
            'tPaid' => 0,
            'cRequested' => 0,
            'cProcessing' => 0,
            'cPaid' => 0
        ];
        if($payments != null){
            foreach ($payments as $payment) {
                if($payment->user->country_is != 'South Africa'){
                    $paymentsCollection->push([
                        'id' => $payment->id,
                        'firstname' => $payment->user->firstname,
                        'paypal_id' => $payment->user->account->wallet,
                        'amount' => $payment->amount,
                        'currency' => 'USD',
                        'country' => $payment->user->country_is,
                        'status' =>  $payment->status_is 
                    ]);
                    if($payment->status_is == 'Requested'){
                        $paymentsMeta['tRequested'] += $payment->amount;
                        $paymentsMeta['cRequested'] ++;
                        array_push($this->requestedPayPalIds, $payment->id); //Save Requested IDs for later processing
                    }
                    elseif($payment->status_is == "Processing"){
                        $paymentsMeta['tProcessing'] += $payment->amount;
                        $paymentsMeta['cProcessing'] ++;
                        array_push($this->processingPayPalIds, $payment->id); //Save Processing IDs for later processing
                    }
                    elseif($payment->status_is == "Paid"){
                        $paymentsMeta['tPaid'] += $payment->amount;
                        $paymentsMeta['cPaid'] ++;
                    }//if status               
                }//if country
            }//foreach
        }

        return view('backend.payments.paypal-payments', compact('page', 'paymentsCollection', 'paymentsMeta'));
    }

    public function process_paypal_clients()
    {
        # code...
    }

    public function sage_payments()
    {
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
    * Function make(), Initiate the payment process 
    * 
    *
    */
    public function make()
    {
        $page=$this->breadcrumbs;
        Session::forget('amount');
        $methods = Payment::$methods;
        $user = Auth::user();
        $amount = round(Invoice::where('user_id', $user->id)->where('status_is', 'Pending')->sum('amount')/14, 2);

        // Check if the amount is in Session
        if(Session::has('amount')) {
            $amount = Session::get('amount');
        } else {
            Session::set('amount',$amount);
            $amount = Session::get('amount');
        }

        if ($amount > 0.00){
          return view('backend.payments.payment-method', compact('methods', 'amount', 'page'));
        }
        else{
          flash('Amount due is '. $amount .', no further action required!', 'blue');
          return redirect('home');
        }
    }

    /**
    * Function process(), processes the payment request using the gateway
    *
    *
    */
    public function process(Request $request)
    {

        $paymentMethod = $request->input('method_is');

        $amount = 0.00;

        if(Session::has('amount')){
            $amount = Session::get('amount'); //get Amount from Session
        }else{
            $amount = Invoice::where('user_id', $user->id)->where('status_is', 'Pending')->sum('amount');
            Session::set('amount', $amount);
            $amount = Session::get('amount');
        }


        if($paymentMethod == 'PayPal'){
            $gateway = $this->getPaypal_gateway();

            //Create a Card Object
            $card = new CreditCard();

            //Set Return and Cancel URL !Required by Gateway
            $returnUrl = url('/payments/finalize');
            $cancelUrl = url('/home');

            $params = array('amount' => $amount,
                'card' => $card,
                'currency' => 'USD',
                'returnUrl' => $returnUrl,
                'cancelUrl' => $cancelUrl );

            session()->put('params', $params); // here you save the params to the session so you can use them later.
            session()->save();

            $response = $gateway->purchase($params)->send();
            //
            if ($response->isRedirect()){
                $response->redirect();
            }else{
                flash('Your payment could not be processed. REASON: ' . $response->getMessage() . ' RECOMMENDED ACTION: Please try again later!', 'red');
                return redirect('home');
            }
        }
        elseif ($paymentMethod == 'Credit') {
            SagePay::setAmount($amount);
            SagePay::setDescription('Lorem ipsum');
            SagePay::setBillingSurname('Mustermann');
            SagePay::setBillingFirstnames('Max');
            SagePay::setBillingCity('Cologne');
            SagePay::setBillingPostCode('412');
            SagePay::setBillingAddress1('88');
            SagePay::setBillingCountry('de');
            SagePay::setDeliverySameAsBilling();
            SagePay::setSuccessURL('http://localhost:8000/home');
            SagePay::setFailureURL('http://localhost:8000/');

            $encrypted_code = SagePay::getCrypt();

            $page=$this->breadcrumbs;
            return view('backend.payments.confirm-sage', compact('encrypted_code', 'page'));

        }
        elseif ($paymentMethod == 'EFT'){
            flash('Banking Details and Payment instructions', 'green');
            return redirect('home');
        }
    }

    /**
    * Function finalize(), finalizes the payment request, this is the last step in the whole process
    *
    *
    */
    public function finalize()
    {
        $gateway = $this->getPaypal_gateway();

        $params = session()->get('params');
        $response = $gateway->completePurchase($params)->send();
        $paypalResponse = $response->getData(); // this is the raw response object

        $user = Auth::user();
        if(isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
            //transaction was successful

            $subscription = Subscription::where(['user_id' => $user->id, 'status_is' => 'Pending'])->first();
            if($subscription){
                $invoice = Invoice::where(['user_id' => $user->id, 'subscription_id' => $subscription->id, 'status_is' => 'Pending'])->first();
            }
            if($invoice) {
                $subscription->status_is='Active';
                $subscription->update(['status_is']);

                $invoice->status_is='Paid';
                $invoice->update(['status_is']);
                
                //Calculate commissions for sponsors
                $amount = $params['amount'];
                $this->calculateCommission($amount);

                flash('Congratulations! Your payment was a success and your subscription has now been activated.', 'green');
                return redirect('home');
            }
            else{
                flash('Something happened while we were trying to activate your subscription. REASON (001): Invoice not found. RECOMMENDED ACTION: Please contact administrator!', 'red darken-4');
                return redirect('home');
            }
        }
        else {
            // Failed transaction ...
            flash('Your payment could not be processed. REASON: Refused by Gateway. RECOMMENDED ACTION: Please try again later.', 'red');
            return redirect('home');
        }
    }

    public function getPaypal_gateway()
    {
        //Initialize Gateway
        $gateway = Omnipay::create('PayPal_Express');

        $gateway_username = env('PAYPAL_EXPRESS_API_USERNAME');
        $gateway_password =  env('PAYPAL_EXPRESS_API_PASSWORD');
        $gateway_signature =  env('PAYPAL_EXPRESS_API_SIGNATURE');

        $gateway->setUsername($gateway_username);
        $gateway->setPassword($gateway_password);
        $gateway->setSignature($gateway_signature);
        $gateway->setTestMode(true);

        return $gateway;
    }
    
    public function calculateCommission($amount)
    {
        $user = Auth::user();
        $parent = User::where('id', $user->referral_id)->get()->first();
        $levels = Level::get();
        $countLevels = $levels->count();
        $index = 0;
        while($parent !== null){
            if($index >= $countLevels){
                break;
            }
            if($user->id != $parent->id){
                Commission::create(['from_user_id' => $user->id, 
                                'to_user_id' => $parent->id, 
                                'amount' => ($amount * ($levels[$index]->percentage/100)),
                                'status_is' => 'Pending']);
            }

            //ToDo: Add to Invoice
            $this->updateInvoice($user, $parent, $amount);

            $parent = User::where('id', $parent->referral_id)->get()->first();
            $index++;
        }
    }

    public function updateInvoice($user, $parent, $amount)
    {
        $invoice = Invoice::where(['user_id' => $parent->id, 'status_is' => 'Pending', 'is_commission' => 1])->first();

        if($invoice == null){
            Invoice::create(['user_id' => $parent->id, 'subscription_id' => $user->id, 
                                'amount' => $amount, 'status_is' => 'Pending', 'is_commission' => 1]);
        }else{
            $sourceIds = explode(',',$invoice->subscription_id);
            array_push($sourceIds, $user->id);

            $totalAmount = $invoice->amount;
            $totalAmount+=$amount;

            $invoice->subscription_id = implode(',',$sourceIds);
            $invoice->amount = $totalAmount;
            
            $invoice->update(['subscription_id', 'amount']);
        }
    }
}
