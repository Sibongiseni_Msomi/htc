<?php

namespace App\Http\Controllers;

use App\Commission;
use App\Invoice;
use App\Signal;
use App\Subscription;
use App\User;
use App\Withdrawal;

use App\Http\Requests;

use Carbon\Carbon;
use Mail;
use Gate;
use Session;
use Illuminate\Http\Request;
use App\Http\Requests\VerifyFormRequest;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public $breadcrumbs = array('page'=>'Dashboard', 'single'=>'Dashboard', 'current'=>'Index', 'header'=>'', 'back'=>'home');
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page=$this->breadcrumbs;
        //Get User
        $user = Auth::user();
        
        //Get User Subscription
        $subscription = Subscription::where(['user_id' => $user->id])->first(); 

        $commission = Commission::where(['to_user_id' => $user->id, 'status_is' => 'Pending'])->sum('amount');
        if($commission == null)
            $commission = 0.00;
        
        // Get amount sum of all pending invoices
        $amount = round(Invoice::where('user_id', $user->id)->where('status_is', 'Pending')->sum('amount')/14, 2);
        if($amount == null)
            $amount = 0;

        // Get all ids of pending commissions for user
        $commissions = Commission::where('to_user_id', $user->id)->where('status_is', 'Pending')->pluck('id')->toArray();
        $commissions = serialize($commissions);

        // Get all signals
        $userSignals = explode(',', Auth::user()->signals);
        if($userSignals !=null)
            $signals = Signal::orderby('created_at','desc')
                ->whereIn('id', array_unique($userSignals))->get();
        
        return view('home', compact('page', 'user', 'signals', 'subscription', 'commission', 'amount', 'commissions'));
    }

    /**
     * Resend verification code.
     *
     * @return \Illuminate\Http\Response
     */
    public function resend()
    {
        // Get user info from Session
        $user = Session::get(Session::get('user_id').':user');
        $username = $user->username;
        $email = $user->email;

        $parameters = array(
            'username' => $username,
            'code' => $user->code,
        );

        Mail::queueOn('ceesonke', 'emails.resend', $parameters, function ($message)
        use ($email, $username) {
            $message->from('noreply@ceesonke.net');
            $message->to($email, $username)->subject('You requested verification code to be resent!');
        });

        flash('You have successfully resent your verification code.', 'green' );

        return redirect('/');
    }

    /**
     * Process verification code submitted.
     *
     * @return \Illuminate\Http\Response
     */
    public function verification(VerifyFormRequest $request)
    {
        $page=$this->breadcrumbs;
        $code = $request['verification'];

        // Get user info from Session
        $user = Auth::user();

        if ($code == $user->code) {
            $user = User::findOrFail($user->id);
            $user->verified = 1;
            Auth::user()->verified = 1;
            $user->update(['verified']);

            $email = $user->email;
            $name = $user->username;

            $parameters = array(
                'username' => $user->username,
            );

            //            // Update user session info
            //            Session::set(Session::get('user_id').':user',$user);
            //
            //            // Send email to confirm successful registration
            //            Mail::queueOn('ceesonke', 'frontend.emails.verified', $parameters, function ($message)
            //            use ($email, $name) {
            //                $message->from('noreply@ceesonke.net');
            //                $message->to($email, $name)->subject('Ceesonke - Profile Verified');
            //            });

            flash('Your profile has been verified.', 'green' );

            //Get User Subscription
            $subscription = Subscription::where(['user_id' => $user->id])->first();

            // Get all signals
            $signals = Signal::orderby('created_at','desc')->get();

            $commission = Commission::where(['to_user_id' => $user->id, 'status_is' => 'Pending'])->sum('amount');
            if($commission == null)
                $commission = 0.00;
            
            // Get amount sum of all pending invoices
            $amount = round(Invoice::where('user_id', $user->id)->where('status_is', 'Pending')->sum('amount')/14, 2);
            if($amount == null)
                $amount = 0;

            // Gell all pending invoices for user
            $invoices = Invoice::where('user_id', $user->id)->where('status_is', 'Pending')->pluck('id')->toArray();
            $invoices = serialize($invoices);

            return view('home', compact('user', 'signals', 'subscription', 'amount', 'commission', 'invoices', 'page'));

        }else{
            flash('Your verification code is incorrect, please try again.', 'red' );
        }

        return redirect('home');
    }

    /**
     * Change personal info.
     *
     * @return \Illuminate\Http\Response
     */
    public function personal(PersonalFormRequest $request)
    {
        // Get user info from Session
        $user = Session::get(Session::get('user_id').':user');
        $user->mobile=$request['mobile'];
        $user->country_is=$request['country_is'];
        $user->update(['mobile','country_is']);

        // Update user session
        Session::set(Session::get('user_id').':user',$user);

        flash('Your personal info has been successfully updated!', 'green');

        return redirect('home');
    }

    /**
     * Change personal info.
     *
     * @return \Illuminate\Http\Response
     */
    public function password(PasswordFormRequest $request)
    {
        // Get user info from Session
        $user = Session::get(Session::get('user_id').':user');
        $user->password=bcrypt($request['password']);
        $user->update(['password']);

        // Update user session
        Session::set(Session::get('user_id').':user',$user);

        flash('Your password has been successfully updated', 'green');

        return redirect('home');
    }

    /**
    * Request a withdrawal
    * @return \Illuminate\Http\Response
    */
    public function withdraw(Request $request)
    {
        if($request['commission'] > 0){
            
            // Determine Payday date
            $thisDay = Carbon::now();
            switch($thisDay->day){
                case ($thisDay->day < 11):
                    $thisDay->day = 15;
                break;
                case ($thisDay->day < 21):
                    $thisDay->day = 25;
                break;
                default:
                    $thisDay->addMonth();
                    $thisDay->day = 15;
            }
            
            // create new Withdrawal
            $withdrawal = Withdrawal::create([
                'user_id'   => Auth::user()->id,
                'commissions'  => $request['commissions'],
                'payday'    => $thisDay,
                'amount'    => $request['commission'],
                'status_is' => 'Requested'
                ]);

            // update the 'Pending' Invoice
            $commissions = unserialize($request['commissions']);
            Commission::whereIn('id', $commissions)->update(['status_is' => 'Processing']);

            flash('Your withdrawal request was created, and your payday is scheduled for ' . $thisDay->formatLocalized('%A %d %b %Y'), 'green' );
            
            return redirect()->action('HomeController@index');
        }

        flash('You don\'t have enough commission to withdraw ', 'red' );
        return redirect('home');
    }
}
