<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests\PasswordFormRequest;
use App\Http\Requests\SubscriptionFormRequest;
use Auth;
use Carbon\Carbon;
use App\Commission;
use App\User;
use App\Invoice;
use App\Package;
use App\Subscription;
use App\Timezone;
use App\Http\Controllers;

use Faker\Factory as Faker;

class UsersController extends Controller
{
    public $breadcrumbs = array('page'=>'Users', 'single'=>'User', 'current'=>'Index', 'header'=>'', 'back'=>'users');
    
    /**
    * Create a new controller instance.
    */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    
    public function index()
    {
        
        $page=$this->breadcrumbs;
        $users = User::get();   
        $sub = Subscription::where('user_id',Auth::user()->id)->first();
        if(!Auth::user()->hasRole('admin')){
            $user = Auth::user();
            $commission = Commission::where(['to_user_id' => $user->id, 'status_is' => 'Pending'])->sum('amount');
            if($commission == null)
                $commission = 0.00;            
             return view('backend.users.index', compact('user', 'page','sub', 'commission'));
        }
        return view('backend.users.index', compact('users', 'page','sub'));
    }
    
    public function show(User $user)
    {
        $page=$this->breadcrumbs;
        $page['current'] = $user->firstname;
        $commission = Commission::where(['to_user_id' => $user->id, 'status_is' => 'Pending'])->sum('amount');
        if($commission == null)
            $commission = 0.00;
        $sub = Subscription::where('user_id',Auth::user()->id)->first();

        return view('backend.users.show', compact('user','sub', 'page', 'commission'));
    }
        
    public function edit(User $user)
    {
        
        $page=$this->breadcrumbs;
        $page['current'] = "Edit";
        $countries = Timezone::pluck('countryName','countryName');
        $timezones = Timezone::pluck('zoneName', 'zoneName');
        $packages = Package::get()->lists('name','id');
        //Adding price to Package list for display
        for ($i = 1; $i < count($packages) + 1; $i++) {
            $daPackage = Package::where('id', $i)->first();
            $packages[$i] = $packages[$i] . ' --- R ' . $daPackage->amount;
        }


        $commission = Commission::where(['to_user_id' => $user->id, 'status_is' => 'Pending'])->sum('amount');
        if($commission == null)
            $commission = 0.00;
        
        return view('backend.users.edit', compact('user','countries','timezones','packages', 'page', 'commission'));
    }
    
    public function update(UserFormRequest $request,User $user)
    {
        $user->update($request->all());
        $user->update(['timezone_offset' => Timezone::where('zoneName', $request['timezone_is'])->pluck('gmtOffset')->first()]);
        return redirect('users');
    }
    
    public function subscription(SubscriptionFormRequest $request)
    {
        //get user info of current Session
        
        $sub = Subscription::where('user_id',Auth::user()->id)->orderBy('id','DESC')->first();
        $sub->package_id = $request['id'];
        $sub->update(['package_id']);
        $package = Package::where('id', $request['id'])->first();
        $invoice = Invoice::where(['subscription_id' => $sub->id, 'status_is' => 'Pending'])->first();
        
        if($invoice == null){
            Invoice::create([
            'user_id' => Auth::user()->id,
            'subscription_id' => $sub->id,
            'status_is' => 'Pending', 
            'amount' => $package->amount,
            ]);
        }else{
            $invoice->amount = $package->amount;
            $invoice->update(['amount']);
        }

        flash("Subscription updated. Invoices may have been affected!", "red darken-1");
        return redirect('users');
    }
    
    public function password(PasswordFormRequest $request)
    {
        //get user info of current Session
        $user = User::where('id', Auth::user()->id)->first();
        $user->password=bcrypt($request['password']);
        $user->update(['password']);
        
        Session::set(Session::get('user_id').':user',$user);
        
        return redirect('users');
    }
}