<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;

use App\Commission;
use App\User;
use App\Withdrawal;


class WithdrawalController extends Controller
{
    public $breadcrumbs = array('page'=>'Withdrawals', 'single'=>'Withdrawal', 'current'=>'Index', 'header'=>'', 'back'=>'withdrawals');
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = $this->breadcrumbs;
        $user = Auth::user();
        $withdrawals = Withdrawal::where('user_id', $user->id)->paginate(4);
        $wcount = $withdrawals->count();
        return view('backend.withdrawals.index', compact('withdrawals', 'page', 'wcount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $page = $this->breadcrumbs;
        $user = Auth::user();
        if($user->hasRole('admin')){
            $withdrawal = Withdrawal::where(['id' => $id])->first();
        }else{
            $withdrawal = Withdrawal::where(['id' => $id, 'user_id' => $user->id])->first();
        }
        
        if($withdrawal){
            $commissions = Commission::whereIn('id', unserialize($withdrawal->commissions))->get();
            $page['current'] = '#' . Withdrawal::where('id', $id)->pluck('id')->first();
        }
        return view('backend.withdrawals.show', compact('page', 'withdrawal', 'commissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
