<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AccountFormRequest;
use Auth;
use App\Account;
use App\Http\Controllers;

class AccountsController extends Controller
{

    public $breadcrumbs = array('page'=>'Account', 'single'=>'Account', 'current'=>'Index', 'header'=>'', 'back'=>'accounts');
    public function __construct()
    {
      $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page=$this->breadcrumbs;
        //$accounts = Account::all();
        $accounts = Account::where('user_id', Auth::user()->id)->get();
        return view('backend.accounts.index', compact('accounts', 'page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $page=$this->breadcrumbs;
        $banks = Account::$banks;
        $statuses = Account::$statuses;
        $types = Account::$types;

        return view('backend.accounts.create', compact('page', 'banks','statuses','types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccountFormRequest $request)
    {
      $account = $request->all();
        // Save the new account
      Auth::user()->account()->create($account);
      //flash()->success('New account has been added!');
      return redirect('accounts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        $page=$this->breadcrumbs;
        if($account->user_id == Auth::user()->id) {
          return view('backend.accounts.show', compact('page', 'account'));
        }
        else {
          return redirect('accounts');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
      $page=$this->breadcrumbs;
      $banks = Account::$banks;
      $statuses = Account::$statuses;
      $types = Account::$types;
      $user = Auth::user()->id;
      //Verifying that the user is verifying the bank details that belong to them
      if($account->user_id == $user){
        return view('backend.accounts.edit', compact('page', 'account','banks','statuses','types'));
      }
      else {
        return redirect('accounts');
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccountFormRequest $request,Account $account)
    {
      $account->update($request->all());
      //flash()->success('Account has been updated!');
      return redirect('accounts');
    }

    public function accounts()
    {
        $page=$this->breadcrumbs;
        $account = Account::where('user_id', Auth::user()->id)->first();
        return view('backend.accounts', compact('page', 'account'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
      $account->delete();
      //flash()->success('Account has been deleted!');
      return redirect('accounts');
    }
}
