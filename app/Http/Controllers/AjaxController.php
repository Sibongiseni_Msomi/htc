<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Signal;
use App\User;

class AjaxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function directReferralsData(Request $request)
    {
        $id = (int)$request->input('id');

        $spillOver = Spillover::where('user_id', $id)->first();

        $array = explode(',', $spillOver->refferals);

        $directReferrals = User::whereIn('id', $array)->get();

        return response()->json([
            'id' => json_encode($id),
            'directReferrals' => json_encode($directReferrals),
        ]);
    }

    public function pushSignal(Request $request)
    {
        $signal = Signal::where('id', (int)$request->input('id'))->first();
        $users = User::where('status_is', 'Active')->get();
        $eligibleUsers = [];
        foreach ($users as $user) {
            if (($user->number_of_signals > 0 && $user->number_of_signals <= 20) || $user->number_of_signals == 100) {
                if ($user->signals != "") {
                    $usersSignals = explode(',', $user->signals);
                    array_push($usersSignals, $signal->id);
                    $user->signals = implode(',', $usersSignals);
                    $user->save();
                } else {
                    $user->signals = $signal->id;
                    $user->save();
                }

                if ($user->number_of_signals > 0 && $user->number_of_signals <= 20) {
                    $user->update(['number_of_signals' => $user->number_of_signals - 1]);
                }
                array_push($eligibleUsers, $user->id);
            }
        }

        return response()->json([
            'eligibleUsers' => json_encode($eligibleUsers),
            'success' => 200,
            'signal' => json_encode($signal),
        ]);
    }
}