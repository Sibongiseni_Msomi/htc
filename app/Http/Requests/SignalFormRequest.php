<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SignalFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'short_message' => 'required',
          'pair_is' => 'required',
          'direction_is' => 'required',
          'action_is' => 'required',
          'entry_is' => 'required',
          'stoploss_is' => 'required',
          'takeprofit_is' => 'required',
          'status_is' => 'required'
      ];
    }
}
