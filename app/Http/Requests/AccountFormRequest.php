<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AccountFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'owner' => 'required|max:60',
            'wallet' => 'required|max:20',
            'code' => 'required|max:8',
        ];
    }

    public function messages()
    {
        return[
            'owner.required' => 'This field is required',
            'wallet.required' => 'This field is required',
            'code.required' => 'This field is required',
        ];
    }
}
