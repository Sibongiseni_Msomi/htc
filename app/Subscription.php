<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_date',
        'expiry_date',
        'user_id',
        'package_id',
        'status_is'];

    public static $statuses= [
        'Pending' => 'Pending',
        'Active' => 'Active',
        'Suspended' => 'Suspended',
        'Blocked' => 'Blocked',
        'Expired' => 'Expired',
    ];

    public function user(){
		return $this->belongsTo('App\User');
    }

    public function package(){
    	return $this->belongsTo('App\Package');
    }

    public function invoice()
    {
        return $this->hasMany('App\Invoice');
    }
}
