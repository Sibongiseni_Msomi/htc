<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //
 	protected $fillable = [
		'user_id',
		'subscription_id',
		'amount',
		'status_is',
		'is_commission'
	];

 	public static $statuses = [
		'Pending' => 'Pending',
		'Processing' => 'Processing',
		'Paid' => 'Paid',
		'Unpaid' => 'Unpaid'
 	];

 	public function user(){
 		return $this->belongsTo('App\User');
 	}

	// An Invoice belongsToMany Package
	public function package()
	{
		return $this->belongsToMany('App\Package')->withPivot(['quantity','unit_price']);
	}

	public function subscription()
	{
		return $this->belongsTo('App\Subscription');
	}
}
