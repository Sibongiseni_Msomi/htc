<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signal extends Model
{
      /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'short_message',
      'pair_is',
      'direction_is',
      'action_is',
      'entry_is',
      'stoploss_is',
      'takeprofit_is',
      'status_is'
    ];

    /**
    * The array of $pairs.
    *
    * @var array
    */
    public static $pairs = [
      'usdzar' => 'USD/ZAR',
      'eurzar' => 'EUR/ZAR',
      'eurusd' => 'EUR/USD'
    ];

    /**
    * The array of $direction.
    *
    * @var array
    */
    public static $direction = [
      'bullish' => 'Bullish',
      'bearish' => 'Bearish'
    ];

    /**
    * The array of $action.
    *
    * @var array
    */
    public static $action = [
      'buy' => 'Buy',
      'sell' => 'Sell',
    ];

    public static $statuses = [
      'active' => 'Active',
      'inactive' => 'Inactive'
    ];
}
