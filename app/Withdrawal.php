<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdrawal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'commissions',
        'payday',
        'amount',
        'status_is'];

    public static $statuses= [
        'Requested' => 'Requested',
        'Processing' => 'Processing',
        'Paid' => 'Paid',
        'Declined' => 'Declined'
    ];

    public function user(){
		return $this->belongsTo('App\User');
    }
}
