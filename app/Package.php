<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    // Disable timestamps
    public $timestamps = false;

    protected $fillable = ['name','features','amount','status_is'];

    public static $statuses = [
    	'Active' => 'Active',
    	'Inactive' => 'Inactive'
    ];

    public function invoice()
    {
        return $this->belongsToMany('App\Invoice')->withPivot(['quantity','unit_price']);
    }

    public function subscription(){
    	return $this->hasMany('App\Subscription');
    }
}
