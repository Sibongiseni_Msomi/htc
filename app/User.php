<?php

namespace App;
use App\Account;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'referral_id', 'position',
        'firstname', 'lastname', 'contactnumber', 'number_of_signals', 'signals', 'email', 'password',
        'verified', 'code', 'status_is', 'country_is', 'timezone_is', 'timezone_offset','number_of_followers'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'code',
    ];

    // A User belongsToMany Role
    public function role()
    {
        return $this->belongsToMany('App\Role');
    }

    // Find out if A User has Role
    public function hasRole($role)
    {
        if(is_string($role)) {
            return $this->role->contains('name', $role);
        }

        return !! $role->intersect($this->role)->count();
    }

    // Assign Role to A User
    public function actAs($role)
    {
        return $this->role()->save(
            Role::whereName($role)->firstOrFail()
        );
    }

    public function account()
    {
      return $this->hasOne('App\Account');
    }

    public function subscription()
    {
        return $this->hasOne('App\Subscription');
    }

    public function withdrawal()
    {
        return $this->hasMany('App\withdrawal');
    }

}
