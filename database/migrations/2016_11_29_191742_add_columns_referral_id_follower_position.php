<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsReferralIdFollowerPosition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function ($table) {
            $table->integer('referral_id')->after('id')->unsigned();
            $table->integer('position')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Remove columns direct_followers and commission
        Schema::table('users', function ($table) {
            $table->dropColumn(['referral_id', 'position']);
        });

    }
}
