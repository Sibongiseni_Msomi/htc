<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('subscriptions', function (Blueprint $table) {
          $table->increments('id');
          $table->date('start_date');
          $table->date('expiry_date');
          $table->integer('user_id')->unsigned();
          $table->integer('package_id')->unsigned();
          $table->string('status_is');
          $table->timestamps();

          $table->foreign('user_id')->references('id')->on('users');
          $table->foreign('package_id')->references('id')->on('packages');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscriptions');
    }
}
