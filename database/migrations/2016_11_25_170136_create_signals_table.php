<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signals', function (Blueprint $table) {
            $table->increments('id');
            $table->text('short_message');
            $table->string('pair_is');
            $table->string('direction_is');
            $table->string('action_is');
            $table->string('entry_is');
            $table->string('stoploss_is');
            $table->string('takeprofit_is');
            $table->string('status_is');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('signals');
    }
}
