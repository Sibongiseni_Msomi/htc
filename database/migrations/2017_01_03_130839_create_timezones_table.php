<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimezonesTable extends Migration
{
    // "countryCode": "AD",
    // "countryName": "Andorra",
    // "zoneName": "Europe\/Andorra",
    // "gmtOffset": 3600,
    // "timestamp": 1483437751
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timezones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('countryCode');
            $table->string('countryName');
            $table->string('zoneName');
            $table->integer('gmtOffset');
            $table->bigInteger('timestamp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('timezones');
    }
}
