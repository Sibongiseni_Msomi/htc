<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create('App\User');
        for ($i=0; $i < 35; $i++) { 
           
        DB::table('users')->insert([
            'referral_id' => 0, 
            'position' => 0,
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'contactnumber' => $faker->e164PhoneNumber,
            'email' => $faker->email,
            'password' => bcrypt('000000'),
            'verified' => 1, 
            'code' => str_random(6), 
            'status_is' => 'Active',
            'country_is' => $faker->country,
            'timezone_is' => $faker->timezone,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),

        ]);
        
        }
    }
}
