<?php

use Illuminate\Database\Seeder;

class SubscriptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('subscriptions')->insert([
          'id' => 1,
          'start_date' => \Carbon\Carbon::now(),
          'expiry_date' => \Carbon\Carbon::now(),
          'user_id' => 1,
          'package_id' => 1,
          'status_is' => 'Active',
          'created_at' => \Carbon\Carbon::now(),
          'updated_at' => \Carbon\Carbon::now(),
      ]);
    }
}
